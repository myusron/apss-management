<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Admin extends CI_Controller
{
  public function __construct()
  {
    parent::__construct();
    if (!$this->session->userdata('email')) {
      redirect('auth');
    }
    $this->load->model('Admin_model');
  }

  public function index()
  {
    $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
    $data['title'] = "Admin";
    $data['countdatauser'] = $this->Admin_model->countDataUser();
    $data['countdatacategories'] = $this->Admin_model->countDataCategories();
    $data['countdatasupliers'] = $this->Admin_model->countDataSupliers();
    $data['countdataunits'] = $this->Admin_model->countDataUnits();
    $data['countdataitems'] = $this->Admin_model->countDataItems();
    $data['countisinput'] = $this->Admin_model->countdataisinput();
    $this->load->view('templates/header', $data);
    $this->load->view('templates/topbar', $data);
    $this->load->view('templates/sidebar', $data);
    $this->load->view('admin/index', $data);
    $this->load->view('templates/footer');
  }

  public function role()
  {
    $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
    $data['title'] = 'Role';
    $data['role'] = $this->db->get('user_role')->result_array();
    $this->load->view('templates/header', $data);
    $this->load->view('templates/sidebar', $data);
    $this->load->view('templates/topbar', $data);
    $this->load->view('admin/role', $data);
    $this->load->view('templates/footer', $data);
  }

  public function roleTambah()
  {
    $data['title'] = 'Tambah Role';  // title
    $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();  // ambil data user
    $this->form_validation->set_rules('role', 'Role', 'required');  // form_validation set_rules
    if ($this->form_validation->run() == false) {  // jika form_validation run
      $this->load->view('templates/header', $data);
      $this->load->view('templates/topbar', $data);
      $this->load->view('templates/sidebar', $data);
      $this->load->view('admin/roletambah', $data);
      $this->load->view('templates/footer', $data);
    } else { // else {model method tambah, session flashdata, redirect role}
      $this->Admin_model->tambahDataRole();
      $this->session->set_flashdata('message', '<div class="alert alert-primary" role="alert">Role berhasil ditambah</div>');
      redirect('admin/role');
    }
  }

  public function roleHapus($id)
  {
    $this->db->delete('user_role', ['id' => $id]);  // delete by id
    $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">Berhasil dihapus</div>');  // session flashdata
    redirect('admin/role');  // redirect
  }

  public function roleUbah($id)
  {
    $data['title'] = 'Ubah Role';  // judul
    $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();  // ambil session user by email
    $data['role'] = $this->Admin_model->ambilDataByIdRole($id);  // ambil/load data by id di model
    $this->form_validation->set_rules('role', 'Role', 'required');  // form_validation set_rules
    if ($this->form_validation->run() == false) {  // jika form_validation run false
      $this->load->view('templates/header', $data);  // jalankan load view
      $this->load->view('templates/topbar', $data);
      $this->load->view('templates/sidebar', $data);
      $this->load->view('admin/roleubah', $data);
      $this->load->view('templates/footer', $data);
    } else { // else {ambil model ubahdata, sesion flashdata, redirect}
      $this->Admin_model->ubahDataRole();
      $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Berhasil diubah</div>');
      redirect('admin/role');
    }
  }

  public function roleaccess($role_id)
  {
    $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
    $data['title'] = 'Role Akses';
    $data['role'] = $this->db->get_where('user_role', ['id' => $role_id])->row_array();

    $data['menu'] = $this->db->get('user_menu')->result_array();

    $this->load->view('templates/header', $data);
    $this->load->view('templates/sidebar', $data);
    $this->load->view('templates/topbar', $data);
    $this->load->view('admin/roleaccess', $data);
    $this->load->view('templates/footer', $data);
  }

  public function changeaccess()
  {
    $menu_id = $this->input->post('menuId');
    $role_id = $this->input->post('roleId');
    $data = [
      'menu_id' => $menu_id,
      'role_id' => $role_id
    ];
    $result = $this->db->get_where('user_access_menu', $data);

    if ($result->num_rows() < 1) {
      $this->db->insert('user_access_menu', $data);
      $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Akses berhasil dirubah</div>');
    } else {
      $this->db->delete('user_access_menu', $data);
      $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Akses berhasil dihapus</div>');
    }
  }

  public function listUser()
  {
    $data['title'] = 'List User';
    $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
    $data['listuser'] = $this->Admin_model->getListUser();

    $this->load->view('templates/header', $data);
    $this->load->view('templates/topbar', $data);
    $this->load->view('templates/sidebar', $data);
    $this->load->view('admin/listuser', $data);
    $this->load->view('templates/footer', $data);
  }

  public function listUserTambah()
  {
    $data['title'] = 'Tambah List User';
    $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
    $this->form_validation->set_rules('name', 'Name', 'required');
    $this->form_validation->set_rules('email', 'Email', 'valid_email|required');
    if ($this->form_validation->run() == false) {
      $this->load->view('templates/header', $data);
      $this->load->view('templates/topbar', $data);
      $this->load->view('templates/sidebar', $data);
      $this->load->view('admin/listusertambah', $data);
      $this->load->view('templates/footer', $data);
    } else {
      $this->Admin_model->tambahDataListUser();
      $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Berhasil menambah data</div>');
      redirect('admin/listuser');
    }
  }

  public function listUserDetail($id)
  {
    $data['title'] = 'Detail User';
    $data['listuserdetail'] = $this->Admin_model->getDataUserById($id);
    $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
    // $data['roleketerangan'] = $this->Admin_model->roleketerangan();
    $this->load->view('templates/header', $data);
    $this->load->view('templates/topbar', $data);
    $this->load->view('templates/sidebar', $data);
    $this->load->view('admin/listuserdetail', $data);
    $this->load->view('templates/footer', $data);
  }

  public function listUserUbah($id)
  {
    $data['title'] = 'List User ubah';
    $data['listuserdetail'] = $this->Admin_model->getDataUserById($id);
    $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
    $data['role'] = $this->db->get('user_role')->result_array();

    $this->form_validation->set_rules('name', 'Name', 'required');
    $this->form_validation->set_rules('email', 'Email', 'required|valid_email');

    if ($this->form_validation->run() == false) {
      $this->load->view('templates/header', $data);
      $this->load->view('templates/topbar', $data);
      $this->load->view('templates/sidebar', $data);
      $this->load->view('admin/listuserubah', $data);
      $this->load->view('templates/footer', $data);
    } else {
      $name = $this->input->post('name');
      $role_id = $this->input->post('role_id');
      $email = $this->input->post('email');
      $upload_image = $_FILES['image']['name'];
      if ($upload_image) {
        $config['allowed_types'] = 'gif|jpg|png';
        $config['max_size'] = '2048';
        $config['upload_path'] = './assets/dist/img/profile';
        $this->load->library('upload', $config);
        if ($this->upload->do_upload('image')) {
          $foto_lama = $data['user']['name'];
          if ($foto_lama != 'default.jpg') {
            unlink(FCPATH . 'assets/dist/img/profile/' . $foto_lama);
          }
          $foto_baru = $this->upload->data('file_name');
          $this->db->set('image', $foto_baru);
        } else {
          echo $this->upload->display_errors();
        }
      }
      $this->db->set('name', $name);
      $this->db->set('role_id', $role_id);
      $this->db->where('email', $email);
      $this->db->update('user');
      $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Berhasil mengedit list user</div>');
      redirect('admin/listuser');
    }
  }

  public function listUserHapus($id)
  {
    $this->db->delete('user', ['id' => $id]);
    $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Berhasil menghapus list user</div>');
    redirect('admin/listuser');
  }
}
