<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Auth extends CI_Controller
{
  public function __construct()
  {
    parent::__construct();
    $this->load->library('form_validation');
  }

  public function index()
  {
    // cek session
    if ($this->session->userdata('email')) {
      redirect('user');
    }
    // setup rule form_validation
    $this->form_validation->set_rules('email', 'Email', 'required|trim|valid_email');
    $this->form_validation->set_rules('password', 'Password', 'required|trim');

    // cek form_validation
    if ($this->form_validation->run() == false) {
      $data['title'] = "Login";
      $this->load->view('templates/header_auth', $data);
      $this->load->view('auth/login', $data);
      $this->load->view('templates/footer_auth');
    } else {
      $this->_login();
    }
  }

  public function _login()
  {
    $email = $this->input->post('email');
    $password = $this->input->post('password');

    $user = $this->db->get_where('user', ['email' => $email])->row_array();
    // jika user nya ada
    if ($user) {
      //jika user aktif
      if ($user['is_active'] = 1) {
        //cek password
        if (password_verify($password, $user['password'])) {
          // siapkan datanya
          $data = [
            'email' => $user['email'],
            'role_id' => $user['role_id']
          ];
          $this->session->set_userdata($data);
          // cek role id
          if ($user['role_id'] == 1) {
            redirect('admin');
          } else {
            redirect('user');
          }
        } else {
          $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">Password salah.</div>');
          redirect('auth');
        }
      } else {
        $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">Email belum diaktivasi</div>');
        redirect('auth');
      }
    } else {
      $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">Email belum registrasi.</div>');
      redirect('auth');
    }
  }

  public function register()
  {
    // cek session
    if ($this->session->userdata('email')) {
      redirect('auth');
    }

    // siapkan ruel saat data ditambahkan
    $this->form_validation->set_rules('name', 'Name', 'required|trim');
    $this->form_validation->set_rules('email', 'Email', 'required|trim|valid_email|is_unique[user.email]');
    $this->form_validation->set_rules('password1', 'Password', 'required|trim|min_length[3]|matches[password2]', [
      'matches' => 'Password tidak sama',
      'min_length' => 'Password terlalu pendek'
    ]);
    $this->form_validation->set_rules('password2', 'Password', 'required|trim|matches[password1]');

    // jalankan validasi
    if ($this->form_validation->run() == false) {
      $data['title'] = "Register";
      $this->load->view('templates/header_auth', $data);
      $this->load->view('auth/register', $data);
      $this->load->view('templates/footer_auth');
    } else {
      $data = [
        'name' => htmlspecialchars($this->input->post('name', true)),
        'email' => htmlspecialchars($this->input->post('email', true)),
        'image' => 'default.jpg',
        'password' => password_hash($this->input->post('password1'), PASSWORD_DEFAULT),
        'role_id' => 2,
        'is_active' => 1,
        'date_created' => time()
      ];
      $this->db->insert('user', $data);
      $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Berhasil registrasi, silahkan login.</div>');
      redirect('auth');
    }
  }

  public function logout()
  {
    $this->session->unset_userdata('email');
    $this->session->unset_userdata('role_id');
    $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Logout berhasil.</div>');
    redirect('auth');
  }
}
