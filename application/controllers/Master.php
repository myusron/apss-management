<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Master extends CI_Controller
{
  public function __construct()
  {
    parent::__construct();
    if (!$this->session->userdata('email')) {
      redirect('auth');
    }

    $this->load->model('Master_model', 'master');
  }

  // start crud a
  public function index()
  {
    $data['title'] = 'Master A';
    $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
    $data['master'] = $this->db->get('master')->result_array();
    $this->load->view('templates/header', $data);
    $this->load->view('templates/topbar', $data);
    $this->load->view('templates/sidebar', $data);
    $this->load->view('master/index', $data);
    $this->load->view('templates/footer', $data);
  }

  public function tambahData()
  {
    $data['title'] = 'Tambah Data Master';
    $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
    $this->form_validation->set_rules('name', 'Name', 'required');
    $this->form_validation->set_rules('phone', 'Phone', 'required');
    $this->form_validation->set_rules('email', 'Email', 'required');
    $this->form_validation->set_rules('address', 'Address', 'required');
    if ($this->form_validation->run() ==  false) {
      $this->load->view('templates/header', $data);
      $this->load->view('templates/topbar', $data);
      $this->load->view('templates/sidebar', $data);
      $this->load->view('master/tambahdata', $data);
      $this->load->view('templates/footer', $data);
    } else {
      $this->db->insert('master', [
        'name' => $this->input->post('name'),
        'phone' => $this->input->post('phone'),
        'email' => $this->input->post('email'),
        'address' => $this->input->post('address')
      ]);
      $this->session->set_flashdata('message', '<div class="alert alert-primary" role="alert">Berhasil menambahkan data baru</div>');
      redirect('master');
    }
  }

  public function delete($id)
  {
    $this->db->delete('master', ['id' => $id]);
    $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Berhasil dihapus</div>');
    redirect('master');
  }

  public function ubah($id) //tangkap data id
  {
    $data['title'] = 'Ubah Data Master';    // judul halaman
    $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();  // ambil session userdata user by email
    $data['master'] = $this->master->getDataById($id);  // ambil data by id dari model

    $this->form_validation->set_rules('name', 'Name', 'required');  // siapin form validasi set_rule
    $this->form_validation->set_rules('phone', 'Phone', 'required');
    $this->form_validation->set_rules('email', 'Email', 'required');
    $this->form_validation->set_rules('address', 'Address', 'required');

    if ($this->form_validation->run() == false) { // jika form_validasi false, redirect master/index
      $this->load->view('templates/header', $data);
      $this->load->view('templates/topbar', $data);
      $this->load->view('templates/sidebar', $data);
      $this->load->view('master/ubahdata', $data);
      $this->load->view('templates/footer', $data);
    } else {  // else {ambil model ubahdata, tampilkan flashdata, dan arahkan ke master/index}
      $this->master->ubahData();
      $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Berhasil diubah</div>');
      redirect('master');
    }
  }

  public function supliers()
  {
    $data['title'] = 'Supliers';
    $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
    $data['supliers'] = $this->db->get('supliers')->result_array();
    $this->load->view('templates/header', $data);
    $this->load->view('templates/topbar', $data);
    $this->load->view('templates/sidebar', $data);
    $this->load->view('master/supliers', $data);
    $this->load->view('templates/footer', $data);
  }

  public function tambahSupliers()
  {
    $data['title'] = 'Tambah Supliers';
    $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
    $data['idbaru'] = $this->master->hitungIdBaru();
    $data['supliers'] = $this->db->get('supliers')->result_array();
    $this->form_validation->set_rules('nama_supliers', 'Nama Supliers', 'required');
    $this->form_validation->set_rules('alamat_supliers', 'Alamat Supliers', 'required');
    $this->form_validation->set_rules('no_telpon', 'NO Telpon', 'required');
    if ($this->form_validation->run() == false) {
      $this->load->view('templates/header', $data);
      $this->load->view('templates/topbar', $data);
      $this->load->view('templates/sidebar', $data);
      $this->load->view('master/tambahsupliers', $data);
      $this->load->view('templates/footer', $data);
    } else {
      $this->db->insert('supliers', [
        'kode_supliers' => $this->input->post('kode_supliers', true),
        'nama_supliers' => $this->input->post('nama_supliers', true),
        'alamat_supliers' => $this->input->post('alamat_supliers', true),
        'no_telpon' => $this->input->post('no_telpon'),
        'is_active' => 1
      ]);
      $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Berhasil menambah supliers</div>');
      redirect('master/supliers');
    }
  }

  public function editSupliers($id)
  {
    $data['title'] = 'Edit Supliers';
    $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
    $data['detailsupliers'] = $this->db->get_where('supliers', ['id' => $id])->row_array();
    $this->form_validation->set_rules('nama_supliers', 'Nama Supliers', 'required');
    $this->form_validation->set_rules('alamat_supliers', 'Alamat Supliers', 'required');
    $this->form_validation->set_rules('no_telpon', 'NO Telpon', 'required');
    if ($this->form_validation->run() == false) {
      $this->load->view('templates/header', $data);
      $this->load->view('templates/topbar', $data);
      $this->load->view('templates/sidebar', $data);
      $this->load->view('master/editsupliers', $data);
      $this->load->view('templates/footer', $data);
    } else {
      $this->db->where('id', $this->input->post('id'));
      $this->db->update('supliers', [
        'kode_supliers' => $this->input->post('kode_supliers', true),
        'nama_supliers' => $this->input->post('nama_supliers', true),
        'alamat_supliers' => $this->input->post('alamat_supliers', true),
        'no_telpon' => $this->input->post('no_telpon'),
        'is_active' => 1
      ]);
      $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Berhasil mengubah supliers</div>');
      redirect('master/supliers');
    }
  }

  public function hapusSupliers($id)
  {
    $this->db->delete('supliers', ['id' => $id]);
    $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Berhasil menghapus supliers</div>');
    redirect('master/supliers');
  }

  public function customers()
  {
    $data['title'] = 'Customers';
    $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
    $data['customers'] = $this->db->get('customers')->result_array();
    $this->load->view('templates/header', $data);
    $this->load->view('templates/topbar', $data);
    $this->load->view('templates/sidebar', $data);
    $this->load->view('master/customers', $data);
    $this->load->view('templates/footer', $data);
  }

  public function tambahCustomers()
  {
    $data['title'] = 'Tambah Customers';
    $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
    $this->form_validation->set_rules('nama', 'Nama', 'required');
    $this->form_validation->set_rules('jenis_kelamin', 'Jenis kelamin', 'required');
    $this->form_validation->set_rules('no_telpon', 'NO Telpon', 'required');
    $this->form_validation->set_rules('alamat', 'Alamat', 'required');
    if ($this->form_validation->run() == false) {
      $this->load->view('templates/header', $data);
      $this->load->view('templates/topbar', $data);
      $this->load->view('templates/sidebar', $data);
      $this->load->view('master/tambahcustomers', $data);
      $this->load->view('templates/footer', $data);
    } else {
      $this->db->insert('customers', [
        'nama' => $this->input->post('nama', true),
        'jenis_kelamin' => $this->input->post('jenis_kelamin', true),
        'no_telpon' => $this->input->post('no_telpon', true),
        'alamat' => $this->input->post('alamat', true)
      ]);
      $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Berhasil menambah customers</div>');
      redirect('master/customers');
    }
  }

  public function editCustomers($id)
  {
    $data['title'] = 'Edit Customers';
    $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
    $data['detailcustomers'] = $this->db->get_where('customers', ['id' => $id])->row_array();
    $this->form_validation->set_rules('nama', 'Nama', 'required');
    $this->form_validation->set_rules('jenis_kelamin', 'Jenis kelamin', 'required');
    $this->form_validation->set_rules('no_telpon', 'NO Telpon', 'required');
    $this->form_validation->set_rules('alamat', 'Alamat', 'required');
    if ($this->form_validation->run() == false) {
      $this->load->view('templates/header', $data);
      $this->load->view('templates/topbar', $data);
      $this->load->view('templates/sidebar', $data);
      $this->load->view('master/editcustomers', $data);
      $this->load->view('templates/footer', $data);
    } else {
      $this->db->where('id', $this->input->post('id'));
      $this->db->update('customers', [
        'nama' => $this->input->post('nama', true),
        'jenis_kelamin' => $this->input->post('jenis_kelamin', true),
        'no_telpon' => $this->input->post('no_telpon', true),
        'alamat' => $this->input->post('alamat', true)
      ]);
      $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Berhasil mengubah customers</div>');
      redirect('master/customers');
    }
  }

  public function hapusCustomers($id)
  {
    $this->db->delete('customers', ['id' => $id]);
    $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Berhasil menghapus customers</div>');
    redirect('master/customers');
  }
}
