<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Menu extends CI_Controller
{
  public function __construct()
  {
    parent::__construct();
    if (!$this->session->userdata('email')) {
      redirect('auth');
    }
    $this->load->model('Menu_model');
  }

  public function index()
  {
    $data['title'] = 'Menu Management';
    $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
    $data['menu'] = $this->db->get('user_menu')->result_array();
    $this->form_validation->set_rules('menu', 'Menu', 'required');
    $this->form_validation->set_rules('icon', 'Icon', 'required');
    if ($this->form_validation->run() == false) {
      $this->load->view('templates/header', $data);
      $this->load->view('templates/topbar', $data);
      $this->load->view('templates/sidebar', $data);
      $this->load->view('menu/index', $data);
      $this->load->view('templates/footer', $data);
    } else {
      $this->db->insert('user_menu', [
        'menu' => $this->input->post('menu'),
        'icon' => $this->input->post('icon')
      ]);
      $this->session->set_flashdata('message', '<div class="alert alert-primary" role="alert">Berhasil menambahkan menu baru</div>');
      redirect('menu');
    }
  }

  public function menuUbah($id)
  {
    $data['title'] = 'Ubah Menu'; // siapin judul
    $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array(); // ambil data user session by email
    $data['menu'] = $this->Menu_model->ambilDataMenuByI($id);  // ambil data by id dari model
    $this->form_validation->set_rules('menu', 'Menu', 'required');  // form_validation set_rules
    $this->form_validation->set_rules('icon', 'Icon', 'required');
    if ($this->form_validation->run() == false) { // jika form_validation run false
      $this->load->view('templates/header', $data);  //load view
      $this->load->view('templates/topbar', $data);
      $this->load->view('templates/sidebar', $data);
      $this->load->view('menu/menuubah', $data);
      $this->load->view('templates/footer', $data);
    } else { //else{load data model -> ubah data -> session set_flashdata -> redirect()}
      $this->Menu_model->ubahDataMenu();
      $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Berhasil diubah</div>');
      redirect('menu');
    }
  }

  public function menuHapus($id)
  {
    $this->db->delete('user_menu', ['id' => $id]);
    $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Berhasil menghapus menu</div>');
    redirect('menu');
  }
  public function submenu()
  {
    $data['title'] = 'Submenu Management';
    $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
    $data['subMenu'] = $this->Menu_model->getSubMenu();  //model join submenu dan menu
    $data['menu'] = $this->db->get('user_menu')->result_array(); //query ambil data user menu
    $this->load->view('templates/header', $data);
    $this->load->view('templates/topbar', $data);
    $this->load->view('templates/sidebar', $data);
    $this->load->view('menu/submenu', $data);
    $this->load->view('templates/footer', $data);
  }

  public function submenuTambah()
  {
    $data['menu'] = $this->db->get('user_menu')->result_array(); //query ambil data user menu
    $data['title'] = 'Submenu tambah';
    $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
    $data['subMenu'] = $this->Menu_model->getSubMenu();
    $this->form_validation->set_rules('menu', 'Menu', 'required');
    $this->form_validation->set_rules('menu_id', 'menu_id', 'required');
    $this->form_validation->set_rules('url', 'Url', 'required');
    $this->form_validation->set_rules('iconsubmenu', 'Iconsubmenu', 'required');
    if ($this->form_validation->run() == false) {
      $this->load->view('templates/header', $data);
      $this->load->view('templates/topbar', $data);
      $this->load->view('templates/sidebar', $data);
      $this->load->view('menu/submenutambah', $data);
      $this->load->view('templates/footer', $data);
    } else {
      $this->Menu_model->submenuTambah();
      $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Berhasil menambahkan sub menu baru</div>');
      redirect('menu/submenu');
    }
  }

  public function submenuHapus($id)
  {
    $this->db->delete('user_sub_menu', ['id' => $id]);
    $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Berhasil menghapus submenu</div>');
    redirect('menu/submenu');
  }

  public function submenuEdit($id)
  {
    $data['title'] = 'Ubah Submenu';
    $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
    $data['menu'] = $this->db->get('user_menu')->result_array();
    $data['submenu'] = $this->Menu_model->submenuById($id);

    $this->form_validation->set_rules('menu', 'Menu', 'required');
    $this->form_validation->set_rules('menu_id', 'menu_id', 'required');
    $this->form_validation->set_rules('url', 'Url', 'required');
    $this->form_validation->set_rules('iconsubmenu', 'Iconsubmenu', 'required');

    if ($this->form_validation->run() == false) {
      $this->load->view('templates/header', $data);
      $this->load->view('templates/topbar', $data);
      $this->load->view('templates/sidebar', $data);
      $this->load->view('menu/submenuedit', $data);
      $this->load->view('templates/sidebar', $data);
    } else {
      $this->Menu_model->editSubmenu();
      $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Berhasil mengedit submenu</div>');
      redirect('menu/submenu');
    }
  }
}
