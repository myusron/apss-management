<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Products extends CI_Controller
{
  public function __construct()
  {
    parent::__construct();
    if (!$this->session->userdata('email')) {
      redirect('auth');
    }
    $this->load->model('Products_model');
  }

  public function categories()
  {
    $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
    $data['title'] = 'Categories';
    $data['categories'] = $this->db->get('categories')->result_array();
    $this->load->view('templates/header', $data);
    $this->load->view('templates/topbar', $data);
    $this->load->view('templates/sidebar', $data);
    $this->load->view('products/categories', $data);
    $this->load->view('templates/footer', $data);
  }

  public function tambahCategories()
  {
    $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
    $data['title'] = 'Tambah Categories';
    $data['categories'] = $this->db->get('categories')->row_array();
    $data['idbaru'] = $this->Products_model->hitungIdBaru();
    $this->form_validation->set_rules('categories', 'Categories', 'required');
    if ($this->form_validation->run() == false) {
      $this->load->view('templates/header', $data);
      $this->load->view('templates/topbar', $data);
      $this->load->view('templates/sidebar', $data);
      $this->load->view('products/tambahcategories', $data);
      $this->load->view('templates/footer', $data);
    } else {
      $this->Products_model->insertDataCategories();
      $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Berhasil menambah categories</div>');
      redirect('products/categories');
    }
  }

  public function editCategories($id)
  {
    $data['title'] = 'Edit categories';
    $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
    $data['categories'] = $this->db->get('categories')->row_array();
    $data['detailcategories'] = $this->Products_model->getCategoriesById($id);
    $this->form_validation->set_rules('categories', 'Categories', 'required');
    if ($this->form_validation->run() == false) {
      $this->load->view('templates/header', $data);
      $this->load->view('templates/topbar', $data);
      $this->load->view('templates/sidebar', $data);
      $this->load->view('products/editcategories', $data);
      $this->load->view('templates/footer', $data);
    } else {
      $this->Products_model->ubahDataCategories();
      $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Berhasil mengubah categories</div>');
      redirect('products/categories');
    }
  }

  public function hapusCategories($id)
  {
    $this->db->delete('categories', ['id' => $id]);
    $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Berhasil menghapus categories</div>');
    redirect('products/categories');
  }

  public function units()
  {
    $data['title'] = 'Units';
    $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
    $data['units'] = $this->db->get('units')->result_array();
    $this->load->view('templates/header', $data);
    $this->load->view('templates/topbar', $data);
    $this->load->view('templates/sidebar', $data);
    $this->load->view('products/units', $data);
    $this->load->view('templates/footer', $data);
  }

  public function tambahUnits()
  {
    $data['title'] = 'Tambah Units';
    $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
    $this->form_validation->set_rules('units', 'Units', 'required');
    if ($this->form_validation->run() == false) {
      $this->load->view('templates/header', $data);
      $this->load->view('templates/topbar', $data);
      $this->load->view('templates/sidebar', $data);
      $this->load->view('products/tambahunits', $data);
      $this->load->view('templates/footer', $data);
    } else {
      $this->db->insert('units', [
        'units' => $this->input->post('units')
      ]);
      $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Berhasil menambah units</div>');
      redirect('products/units');
    }
  }

  public function editUnits($id)
  {
    $data['title'] = 'Edit Units';
    $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
    $data['units'] = $this->db->get_where('units', ['id' => $id])->row_array();
    $this->form_validation->set_rules('units', 'Units', 'required');
    if ($this->form_validation->run() == false) {
      $this->load->view('templates/header', $data);
      $this->load->view('templates/topbar', $data);
      $this->load->view('templates/sidebar', $data);
      $this->load->view('products/editunits', $data);
      $this->load->view('templates/footer', $data);
    } else {
      $this->db->where('id', $this->input->post('id'));
      $this->db->update('units', [
        'units' => $this->input->post('units')
      ]);
      $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Berhasil mengubah units</div>');
      redirect('products/units');
    }
  }

  public function hapusUnits($id)
  {
    $this->db->delete('units', ['id' => $id]);
    $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Berhasil menghapus units</div>');
    redirect('units');
  }

  public function items()
  {
    $data['title'] = 'Items';
    $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
    $data['items'] = $this->db->get('items')->result_array();
    $data['dataitems'] = $this->Products_model->joinDataItems();
    $this->load->view('templates/header', $data);
    $this->load->view('templates/topbar', $data);
    $this->load->view('templates/sidebar', $data);
    $this->load->view('products/items', $data);
    $this->load->view('templates/footer', $data);
  }

  public function tambahItems()
  {
    $data['title'] = 'Tambah Items';
    $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
    $data['iditems'] = $this->Products_model->hitungiditems();
    $data['supliers'] = $this->db->get('supliers')->result_array();
    $data['categories'] = $this->db->get('categories')->result_array();
    $data['units'] = $this->db->get('units')->result_array();
    $this->form_validation->set_rules('nama_items', 'Nama Items', 'required');
    $this->form_validation->set_rules('harga', 'Harga', 'required');
    if ($this->form_validation->run() == false) {
      $this->load->view('templates/header', $data);
      $this->load->view('templates/topbar', $data);
      $this->load->view('templates/sidebar', $data);
      $this->load->view('products/tambahitems', $data);
      $this->load->view('templates/footer', $data);
    } else {
      $this->db->insert('items', [
        'kode_items' => $this->input->post('kode_items'),
        'nama_items' => $this->input->post('nama_items'),
        'deskripsi' => $this->input->post('deskripsi'),
        'harga' => $this->input->post('harga'),
        'supliers_id' => $this->input->post('supliers_id'),
        'categories_id' => $this->input->post('categories_id'),
        'units_id' => $this->input->post('units_id')
      ]);
      $this->session->set_flashdata('message', '<div class="alert alert-primary" role="alert">Berhasil menambah items</div>');
      redirect('products/items');
    }
  }

  public function editItems($id)
  {
    $data['title'] = 'Edit Items';
    $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
    $data['items'] = $this->db->get_where('items', ['id' => $id])->row_array();
    $data['supliers'] = $this->db->get('supliers')->result_array();
    $data['categories'] = $this->db->get('categories')->result_array();
    $data['units'] = $this->db->get('units')->result_array();
    $this->form_validation->set_rules('nama_items', 'Nama Items', 'required');
    if ($this->form_validation->run() == false) {
      $this->load->view('templates/header', $data);
      $this->load->view('templates/topbar', $data);
      $this->load->view('templates/sidebar', $data);
      $this->load->view('products/edititems', $data);
      $this->load->view('templates/footer', $data);
    } else {
      $this->db->where('id', $this->input->post('id', true));
      $this->db->update('items', [
        'kode_items' => $this->input->post('kode_items', true),
        'nama_items' => $this->input->post('nama_items', true),
        'deskripsi' => $this->input->post('deskripsi', true),
        'harga' => $this->input->post('harga', true),
        'supliers_id' => $this->input->post('supliers_id', true),
        'categories_id' => $this->input->post('categories_id', true),
        'units_id' => $this->input->post('units_id', true)
      ]);
      $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Berhasil mengubah items</div>');
      redirect('products/items');
    }
  }

  public function hapusItems($id)
  {
    $this->db->delete('items', ['id' => $id]);
    $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">Berhasil menghapus items</div>');
    redirect('products/items');
  }
}
