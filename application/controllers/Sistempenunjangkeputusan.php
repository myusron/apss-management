<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Sistempenunjangkeputusan extends CI_Controller
{
  public function __construct()
  {
    parent::__construct();
    if (!$this->session->userdata('email')) {
      redirect('auth');
    }
    $this->load->model('Sistempenunjangkeputusan_model', 'spk');
  }


  // kepentingan kriteria
  public function kepentingankriteria()
  {
    $data['title'] = 'Kepentingan Kriteria';
    $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
    $data['kepentingankriteria'] = $this->db->get('kepentingan_kriteria')->result_array();
    $this->load->view('templates/header', $data);
    $this->load->view('templates/topbar', $data);
    $this->load->view('templates/sidebar', $data);
    $this->load->view('sistempenunjangkeputusan/kepentingankriteria', $data);
    $this->load->view('templates/footer', $data);
  }
  public function tambahkepentingankriteria()
  {
    $data['title'] = 'Tambah Kepentingan Kriteria';
    $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
    $this->form_validation->set_rules('bobot', 'Bobot', 'required');
    $this->form_validation->set_rules('kepentingan', 'Kepentingan', 'required');
    if ($this->form_validation->run() == false) {
      $this->load->view('templates/header', $data);
      $this->load->view('templates/topbar', $data);
      $this->load->view('templates/sidebar', $data);
      $this->load->view('sistempenunjangkeputusan/tambahkepentingankriteria', $data);
      $this->load->view('templates/footer', $data);
    } else {
      $this->db->insert('kepentingan_kriteria', [
        'bobot' => $this->input->post('bobot'),
        'kepentingan' => $this->input->post('kepentingan')
      ]);
      $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Berhasil menambah kepentingan kriteria</div>');
      redirect('sistempenunjangkeputusan/kepentingankriteria');
    }
  }
  public function editkepentingankriteria($id)
  {
    $data['title'] = 'Edit Kepentingan Kriteria';
    $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
    $data['kepkri'] = $this->db->get_where('kepentingan_kriteria', ['id' => $id])->row_array();
    $this->form_validation->set_rules('bobot', 'Bobot', 'required');
    $this->form_validation->set_rules('kepentingan', 'Kepentingan', 'required');
    if ($this->form_validation->run() == false) {
      $this->load->view('templates/header', $data);
      $this->load->view('templates/topbar', $data);
      $this->load->view('templates/sidebar', $data);
      $this->load->view('sistempenunjangkeputusan/editkepentingankriteria', $data);
      $this->load->view('templates/footer', $data);
    } else {
      $this->db->where('id', $this->input->post('id'));
      $this->db->update('kepentingan_kriteria', [
        'bobot' => $this->input->post('bobot'),
        'kepentingan' => $this->input->post('kepentingan')
      ]);
      $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Berhasil mengubah kepentingan kriteria</div>');
      redirect('sistempenunjangkeputusan/kepentingankriteria');
    }
  }
  public function hapusKepentingankriteria($id)
  {
    $this->db->delete('kepentingan_kriteria', ['id' => $id]);
    $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Berhasil menghapus kepentingan kriteria</div>');
    redirect('sistempenunjangkeputusan/kepentingankriteria');
  }


  // tipe kriteria
  public function tipekriteria()
  {
    $data['title'] = 'Tipe Kriteria';
    $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
    $data['tipekriteria'] = $this->db->get('tipe_kriteria')->result_array();
    $this->load->view('templates/header', $data);
    $this->load->view('templates/topbar', $data);
    $this->load->view('templates/sidebar', $data);
    $this->load->view('sistempenunjangkeputusan/tipekriteria', $data);
    $this->load->view('templates/footer', $data);
  }
  public function tambahtipekriteria()
  {
    $data['title'] = 'Tambah Tipe Kriteria';
    $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
    $this->form_validation->set_rules('kriteria', 'kriteria', 'required');
    $this->form_validation->set_rules('tipe', 'tipe', 'required');
    if ($this->form_validation->run() == false) {
      $this->load->view('templates/header', $data);
      $this->load->view('templates/topbar', $data);
      $this->load->view('templates/sidebar', $data);
      $this->load->view('sistempenunjangkeputusan/tambahtipekriteria', $data);
      $this->load->view('templates/footer', $data);
    } else {
      $this->db->insert('tipe_kriteria', [
        'kriteria' => $this->input->post('kriteria'),
        'tipe' => $this->input->post('tipe')
      ]);
      $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Berhasil mengubah tipe kriteria</div>');
      redirect('sistempenunjangkeputusan/tipekriteria');
    }
  }
  public function edittipekriteria($id)
  {
    $data['title'] = 'Edit Tipe Kriteria';
    $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
    $data['tipekriteria'] = $this->db->get_where('tipe_kriteria', ['id' => $id])->row_array();
    $this->form_validation->set_rules('kriteria', 'kriteria', 'required');
    $this->form_validation->set_rules('tipe', 'tipe', 'required');
    if ($this->form_validation->run() == false) {
      $this->load->view('templates/header', $data);
      $this->load->view('templates/topbar', $data);
      $this->load->view('templates/sidebar', $data);
      $this->load->view('sistempenunjangkeputusan/edittipekriteria', $data);
      $this->load->view('templates/footer', $data);
    } else {
      $this->db->where('id', $this->input->post('id'));
      $this->db->update('tipe_kriteria', [
        'kriteria' => $this->input->post('kriteria'),
        'tipe' => $this->input->post('tipe')
      ]);
      $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Berhasil mengubah tipe kriteria</div>');
      redirect('sistempenunjangkeputusan/tipekriteria');
    }
  }
  public function hapusTipekriteria($id)
  {
    $this->db->delete('tipe_kriteria', ['id' => $id]);
    $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Berhasil menghapus tipe kriteria</div>');
    redirect('sistempenunjangkeputusan/tipekriteria');
  }


  // pembobotan
  public function pembobotan()
  {
    $data['title'] = 'Pembobotan';
    $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
    $data['pembobotan'] = $this->db->get('pembobotan')->result_array();
    $data['joinpembobotankriteria'] = $this->spk->joinPembobotanKriteria();
    $this->load->view('templates/header', $data);
    $this->load->view('templates/topbar', $data);
    $this->load->view('templates/sidebar', $data);
    $this->load->view('sistempenunjangkeputusan/pembobotan', $data);
    $this->load->view('templates/footer', $data);
  }
  public function tambahpembobotan()
  {
    $data['title'] = 'Tambah Pembobotan';
    $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
    $data['joinpembobotankriteria'] = $this->spk->joinPembobotanKriteria();
    $data['tipekriteria'] = $this->db->get('tipe_kriteria')->result_array();
    $data['kepentingankriteria'] = $this->db->get('kepentingan_kriteria')->result_array();
    $this->form_validation->set_rules('tipe_kriteria_id', 'Tipe Kriteria', 'required');
    $this->form_validation->set_rules('kepentingan_kriteria_id', 'Kepentingan Kriteria', 'required');
    if ($this->form_validation->run() == false) {
      $this->load->view('templates/header', $data);
      $this->load->view('templates/topbar', $data);
      $this->load->view('templates/sidebar', $data);
      $this->load->view('sistempenunjangkeputusan/tambahpembobotan', $data);
      $this->load->view('templates/footer', $data);
    } else {
      $this->db->insert('pembobotan', [
        'tipe_kriteria_id' => $this->input->post('tipe_kriteria_id'),
        'kepentingan_kriteria_id' => $this->input->post('kepentingan_kriteria_id')
      ]);
      $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Berhasil menambah pembobotan</div>');
      redirect('sistempenunjangkeputusan/pembobotan');
    }
  }
  public function editpembobotan($id)
  {
    $data['title'] = 'Edit Pembobotan';
    $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
    $data['pembobotan'] = $this->db->get_where('pembobotan', ['id' => $id])->row_array();
    $data['joinpembobotankriteria'] = $this->spk->joinPembobotanKriteria();
    $data['tipekriteria'] = $this->db->get('tipe_kriteria')->result_array();
    $data['kepentingankriteria'] = $this->db->get('kepentingan_kriteria')->result_array();
    $this->form_validation->set_rules('tipe_kriteria_id', 'Tipe Kriteria', 'required');
    $this->form_validation->set_rules('kepentingan_kriteria_id', 'Kepentingan Kriteria', 'required');
    if ($this->form_validation->run() == false) {
      $this->load->view('templates/header', $data);
      $this->load->view('templates/topbar', $data);
      $this->load->view('templates/sidebar', $data);
      $this->load->view('sistempenunjangkeputusan/editpembobotan', $data);
      $this->load->view('templates/footer', $data);
    } else {
      $this->db->where('id', $this->input->post('id'));
      $this->db->update('pembobotan', [
        'tipe_kriteria_id' => $this->input->post('tipe_kriteria_id'),
        'kepentingan_kriteria_id' => $this->input->post('kepentingan_kriteria_id')
      ]);
      $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Berhasil mengubah pembobotan</div>');
      redirect('sistempenunjangkeputusan/pembobotan');
    }
  }
  public function hapusPembobotan($id)
  {
    $this->db->delete('pembobotan', ['id' => $id]);
    $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Berhasil menghapus pembobotan</div>');
    redirect('sistempenunjangkeputusan/pembobotan');
  }


  public function lamarankerja()
  {
    $data['title'] = 'Daftar Alternatif';
    $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
    $this->load->model('Admin_model');
    $data['listlamarankerja'] = $this->Admin_model->getAllPelamar();
    $this->load->view('templates/header', $data);
    $this->load->view('templates/topbar', $data);
    $this->load->view('templates/sidebar', $data);
    $this->load->view('sistempenunjangkeputusan/lamarankerja', $data);
    $this->load->view('templates/footer', $data);
  }
  public function hapusLamarankerja($id)
  {
    $this->db->delete('lamaran_kerja', ['id' => $id]);
    $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Berhasil mengapus lamaran kerja</div>');
    redirect('sistempenunjangkeputusan/lamarankerja');
  }
  public function editlamarankerja($id)
  {
    $data['title'] = 'Input Kriteria';
    $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
    $data['kriteria'] = $this->db->get_where('lamaran_kerja', ['id' => $id])->row_array();
    $this->form_validation->set_rules('C1', 'C1', 'required');
    if ($this->form_validation->run() == false) {
      $this->load->view('templates/header', $data);
      $this->load->view('templates/topbar', $data);
      $this->load->view('templates/sidebar', $data);
      $this->load->view('sistempenunjangkeputusan/editlamarankerja', $data);
      $this->load->view('templates/footer', $data);
    } else {
      $this->db->where('id', $this->input->post('id'));
      $this->db->update('lamaran_kerja', [
        'C1' => $this->input->post('C1'),
        'C2' => $this->input->post('C2'),
        'C3' => $this->input->post('C3'),
        'C4' => $this->input->post('C4'),
        'C5' => $this->input->post('C5'),
        'C6' => $this->input->post('C6'),
        'C7' => $this->input->post('C7'),
        'C8' => $this->input->post('C8'),
        'is_input' => 1
      ]);
      $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Berhasil input kriteria</div>');
      redirect('sistempenunjangkeputusan/lamarankerja');
    }
  }


  // dashboard
  public function dashboard()
  {
    $data['title'] = 'Dashboard';
    $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();

    $data['pembobotan'] = $this->db->get('pembobotan')->result_array();
    $data['joinpembobotankriteria'] = $this->spk->joinPembobotanKriteria();

    $data['tipekriteria'] = $this->db->get('tipe_kriteria')->result_array();

    $data['kepentingankriteria'] = $this->db->get('kepentingan_kriteria')->result_array();

    $this->load->model('Admin_model');
    $data['countisinput'] = $this->Admin_model->countdataisinput();

    $data['listlamarankerja'] = $this->Admin_model->listjoin();

    $data['countbobot'] = $this->spk->countPembobotan();

    $data['kepentingankriteriaid1'] = $this->spk->kepentinganKriteriaId1();
    $data['kepentingankriteriaid2'] = $this->spk->kepentinganKriteriaId2();
    $data['kepentingankriteriaid3'] = $this->spk->kepentinganKriteriaId3();
    $data['kepentingankriteriaid4'] = $this->spk->kepentinganKriteriaId4();
    $data['kepentingankriteriaid5'] = $this->spk->kepentinganKriteriaId5();
    $data['kepentingankriteriaid6'] = $this->spk->kepentinganKriteriaId6();
    $data['kepentingankriteriaid7'] = $this->spk->kepentinganKriteriaId7();
    $data['kepentingankriteriaid8'] = $this->spk->kepentinganKriteriaId8();

    $data['C1id5'] = $this->spk->C1id5();
    $data['C2id5'] = $this->spk->C2id5();
    $data['C3id5'] = $this->spk->C3id5();
    $data['C4id5'] = $this->spk->C4id5();
    $data['C5id5'] = $this->spk->C5id5();
    $data['C6id5'] = $this->spk->C6id5();
    $data['C7id5'] = $this->spk->C7id5();
    $data['C8id5'] = $this->spk->C8id5();

    $data['C1id6'] = $this->spk->C1id6();
    $data['C2id6'] = $this->spk->C2id6();
    $data['C3id6'] = $this->spk->C3id6();
    $data['C4id6'] = $this->spk->C4id6();
    $data['C5id6'] = $this->spk->C5id6();
    $data['C6id6'] = $this->spk->C6id6();
    $data['C7id6'] = $this->spk->C7id6();
    $data['C8id6'] = $this->spk->C8id6();

    $data['C1id7'] = $this->spk->C1id7();
    $data['C2id7'] = $this->spk->C2id7();
    $data['C3id7'] = $this->spk->C3id7();
    $data['C4id7'] = $this->spk->C4id7();
    $data['C5id7'] = $this->spk->C5id7();
    $data['C6id7'] = $this->spk->C6id7();
    $data['C7id7'] = $this->spk->C7id7();
    $data['C8id7'] = $this->spk->C8id7();

    $data['C1id8'] = $this->spk->C1id8();
    $data['C2id8'] = $this->spk->C2id8();
    $data['C3id8'] = $this->spk->C3id8();
    $data['C4id8'] = $this->spk->C4id8();
    $data['C5id8'] = $this->spk->C5id8();
    $data['C6id8'] = $this->spk->C6id8();
    $data['C7id8'] = $this->spk->C7id8();
    $data['C8id8'] = $this->spk->C8id8();

    $data['C1id9'] = $this->spk->C1id9();
    $data['C2id9'] = $this->spk->C2id9();
    $data['C3id9'] = $this->spk->C3id9();
    $data['C4id9'] = $this->spk->C4id9();
    $data['C5id9'] = $this->spk->C5id9();
    $data['C6id9'] = $this->spk->C6id9();
    $data['C7id9'] = $this->spk->C7id9();
    $data['C8id9'] = $this->spk->C8id9();

    $data['C1id10'] = $this->spk->C1id10();
    $data['C2id10'] = $this->spk->C2id10();
    $data['C3id10'] = $this->spk->C3id10();
    $data['C4id10'] = $this->spk->C4id10();
    $data['C5id10'] = $this->spk->C5id10();
    $data['C6id10'] = $this->spk->C6id10();
    $data['C7id10'] = $this->spk->C7id10();
    $data['C8id10'] = $this->spk->C8id10();

    $data['C1id11'] = $this->spk->C1id11();
    $data['C2id11'] = $this->spk->C2id11();
    $data['C3id11'] = $this->spk->C3id11();
    $data['C4id11'] = $this->spk->C4id11();
    $data['C5id11'] = $this->spk->C5id11();
    $data['C6id11'] = $this->spk->C6id11();
    $data['C7id11'] = $this->spk->C7id11();
    $data['C8id11'] = $this->spk->C8id11();

    $this->load->view('templates/header', $data);
    $this->load->view('templates/topbar', $data);
    $this->load->view('templates/sidebar', $data);
    $this->load->view('sistempenunjangkeputusan/dashboard', $data);
    $this->load->view('templates/footer', $data);
  }

  public function bobotkriteria()
  {
    $data['title'] = 'Bobot Kriteria';
    $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
    $data['bobotkriteria'] = $this->db->get('bobot_kriteria')->result_array();
    $data['countbobot'] = $this->spk->countPembobotan();
    $data['kepentingankriteria'] = $this->spk->kepentinganKriteria();
    $data['kepentingankriteriaid1'] = $this->spk->kepentinganKriteriaId1();
    $data['kepentingankriteriaid2'] = $this->spk->kepentinganKriteriaId2();
    $data['kepentingankriteriaid3'] = $this->spk->kepentinganKriteriaId3();
    $data['kepentingankriteriaid4'] = $this->spk->kepentinganKriteriaId4();
    $data['kepentingankriteriaid5'] = $this->spk->kepentinganKriteriaId5();
    $data['kepentingankriteriaid6'] = $this->spk->kepentinganKriteriaId6();
    $data['kepentingankriteriaid7'] = $this->spk->kepentinganKriteriaId7();
    $data['kepentingankriteriaid8'] = $this->spk->kepentinganKriteriaId8();
    $this->load->view('templates/header', $data);
    $this->load->view('templates/topbar', $data);
    $this->load->view('templates/sidebar', $data);
    $this->load->view('sistempenunjangkeputusan/bobotkriteria', $data);
    $this->load->view('templates/footer', $data);
  }
  // public function tambahkepentingankriteria()
  // {
  //   $data['title'] = 'Tambah Kepentingan Kriteria';
  //   $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
  //   $this->form_validation->set_rules('bobot', 'Bobot', 'required');
  //   $this->form_validation->set_rules('kepentingan', 'Kepentingan', 'required');
  //   if ($this->form_validation->run() == false) {
  //     $this->load->view('templates/header', $data);
  //     $this->load->view('templates/topbar', $data);
  //     $this->load->view('templates/sidebar', $data);
  //     $this->load->view('sistempenunjangkeputusan/tambahkepentingankriteria', $data);
  //     $this->load->view('templates/footer', $data);
  //   } else {
  //     $this->db->insert('kepentingan_kriteria', [
  //       'bobot' => $this->input->post('bobot'),
  //       'kepentingan' => $this->input->post('kepentingan')
  //     ]);
  //     $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Berhasil menambah kepentingan kriteria</div>');
  //     redirect('sistempenunjangkeputusan/kepentingankriteria');
  //   }
  // }
  // public function editkepentingankriteria($id)
  // {
  //   $data['title'] = 'Edit Kepentingan Kriteria';
  //   $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
  //   $data['kepkri'] = $this->db->get_where('kepentingan_kriteria', ['id' => $id])->row_array();
  //   $this->form_validation->set_rules('bobot', 'Bobot', 'required');
  //   $this->form_validation->set_rules('kepentingan', 'Kepentingan', 'required');
  //   if ($this->form_validation->run() == false) {
  //     $this->load->view('templates/header', $data);
  //     $this->load->view('templates/topbar', $data);
  //     $this->load->view('templates/sidebar', $data);
  //     $this->load->view('sistempenunjangkeputusan/editkepentingankriteria', $data);
  //     $this->load->view('templates/footer', $data);
  //   } else {
  //     $this->db->where('id', $this->input->post('id'));
  //     $this->db->update('kepentingan_kriteria', [
  //       'bobot' => $this->input->post('bobot'),
  //       'kepentingan' => $this->input->post('kepentingan')
  //     ]);
  //     $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Berhasil mengubah kepentingan kriteria</div>');
  //     redirect('sistempenunjangkeputusan/kepentingankriteria');
  //   }
  // }
  // public function hapusKepentingankriteria($id)
  // {
  //   $this->db->delete('kepentingan_kriteria', ['id' => $id]);
  //   $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Berhasil menghapus kepentingan kriteria</div>');
  //   redirect('sistempenunjangkeputusan/kepentingankriteria');
  // }
}
