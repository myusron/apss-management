<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Spk extends CI_Controller
{
  public function __construct()
  {
    parent::__construct();
    if (!$this->session->userdata('email')) {
      redirect('auth');
    }
  }

  public function alternatif()
  {
    $data['title'] = 'Alternatif';
    $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
    $data['alternatif'] = $this->db->get('spk_alternatif')->result_array();
    $this->load->view('templates/header', $data);
    $this->load->view('templates/topbar', $data);
    $this->load->view('templates/sidebar', $data);
    $this->load->view('spk/alternatif', $data);
    $this->load->view('templates/footer', $data);
  }
  public function tambahAlternatif()
  {
    $data['title'] = 'Tambah Alternatif';
    $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
    $this->form_validation->set_rules('alternatif', 'Alternatif', 'required');
    $this->form_validation->set_rules('keterangan', 'Keterangan', 'required');
    if ($this->form_validation->run() == false) {
      $this->load->view('templates/header', $data);
      $this->load->view('templates/topbar', $data);
      $this->load->view('templates/sidebar', $data);
      $this->load->view('spk/tambahalternatif', $data);
      $this->load->view('templates/footer', $data);
    } else {
      $this->db->insert('spk_alternatif', [
        'alternatif' => $this->input->post('alternatif', true),
        'keterangan' => $this->input->post('keterangan', true)
      ]);
      $this->session->set_flashdata('message', '<div class="alert alert-primary" role="alert">Alternatif berhasil ditambah.</div>');
      redirect('spk/alternatif');
    }
  }
  public function editAlternatif($id)
  {
    $data['title'] = 'Edit Alternatif';
    $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
    $data['alternatif'] = $this->db->get_where('spk_alternatif', ['id' => $id])->row_array();
    $this->form_validation->set_rules('alternatif', 'Alternatif', 'required');
    $this->form_validation->set_rules('keterangan', 'Keterangan', 'required');
    if ($this->form_validation->run() == false) {
      $this->load->view('templates/header', $data);
      $this->load->view('templates/topbar', $data);
      $this->load->view('templates/sidebar', $data);
      $this->load->view('spk/editalternatif', $data);
      $this->load->view('templates/footer', $data);
    } else {
      $this->db->where('id', $this->input->post('id'));
      $this->db->update('spk_alternatif', [
        'alternatif' => $this->input->post('alternatif', true),
        'keterangan' => $this->input->post('keterangan', true)
      ]);
      $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Alternatif berhasil diubah.</div>');
      redirect('spk/alternatif');
    }
  }
  public function deleteAlternatif($id)
  {
    $this->db->delete('spk_alternatif', ['id' => $id]);
    $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">Alternatif berhasil dihapus.</div>');
    redirect('spk/alternatif');
  }

  public function bobot()
  {
    $data['title'] = 'Bobot';
    $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
    $data['bobot'] = $this->db->get('spk_bobot')->result_array();
    $this->load->view('templates/header', $data);
    $this->load->view('templates/topbar', $data);
    $this->load->view('templates/sidebar', $data);
    $this->load->view('spk/bobot', $data);
    $this->load->view('templates/footer', $data);
  }
  public function tambahBobot()
  {
    $data['title'] = 'Tambah Bobot';
    $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
    $this->form_validation->set_rules('bobot', 'Bobot', 'required');
    $this->form_validation->set_rules('kepentingan', 'Kepentingan', 'required');
    if ($this->form_validation->run() == false) {
      $this->load->view('templates/header', $data);
      $this->load->view('templates/topbar', $data);
      $this->load->view('templates/sidebar', $data);
      $this->load->view('spk/tambahbobot', $data);
      $this->load->view('templates/footer', $data);
    } else {
      $this->db->insert('spk_bobot', [
        'bobot' => $this->input->post('bobot', true),
        'kepentingan' => $this->input->post('kepentingan', true)
      ]);
      $this->session->set_flashdata('message', '<div class="alert alert-primary" role="alert">Bobot berhasil ditambah.</div>');
      redirect('spk/bobot');
    }
  }
  public function editBobot($id)
  {
    $data['title'] = 'Edit Bobot';
    $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
    $data['bobot'] = $this->db->get_where('spk_bobot', ['id' => $id])->row_array();
    $this->form_validation->set_rules('bobot', 'Bobot', 'required');
    $this->form_validation->set_rules('kepentingan', 'Kepentingan', 'required');
    if ($this->form_validation->run() == false) {
      $this->load->view('templates/header', $data);
      $this->load->view('templates/topbar', $data);
      $this->load->view('templates/sidebar', $data);
      $this->load->view('spk/editbobot', $data);
      $this->load->view('templates/footer', $data);
    } else {
      $this->db->where('id', $this->input->post('id'));
      $this->db->update('spk_bobot', [
        'bobot' => $this->input->post('bobot', true),
        'kepentingan' => $this->input->post('kepentingan', true)
      ]);
      $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Bobot berhasil diubah.</div>');
      redirect('spk/bobot');
    }
  }
  public function deleteBobot($id)
  {
    $this->db->delete('spk_bobot', ['id' => $id]);
    $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">Bobot berhasil dihapus.</div>');
    redirect('spk/bobot');
  }

  public function kriteria()
  {
    $data['title'] = 'Kriteria';
    $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
    $data['kriteria'] = $this->db->get('spk_kriteria')->result_array();
    $this->load->view('templates/header', $data);
    $this->load->view('templates/topbar', $data);
    $this->load->view('templates/sidebar', $data);
    $this->load->view('spk/kriteria', $data);
    $this->load->view('templates/footer', $data);
  }
  public function tambahKriteria()
  {
    $data['title'] = 'Tambah Kriteria';
    $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
    $this->form_validation->set_rules('kriteria', 'Kriteria', 'required');
    $this->form_validation->set_rules('kode', 'Kode', 'required');
    $this->form_validation->set_rules('tipe', 'Tipe', 'required');
    if ($this->form_validation->run() == false) {
      $this->load->view('templates/header', $data);
      $this->load->view('templates/topbar', $data);
      $this->load->view('templates/sidebar', $data);
      $this->load->view('spk/tambahkriteria', $data);
      $this->load->view('templates/footer', $data);
    } else {
      $this->db->insert('spk_kriteria', [
        'kriteria' => $this->input->post('kriteria', true),
        'kode' => $this->input->post('kode', true),
        'tipe' => $this->input->post('tipe', true)
      ]);
      $this->session->set_flashdata('message', '<div class="alert alert-primary" role="alert">Kriteria berhasil ditambah.</div>');
      redirect('spk/kriteria');
    }
  }
  public function editKriteria($id)
  {
    $data['title'] = 'Edit Kriteria';
    $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
    $data['kriteria'] = $this->db->get_where('spk_kriteria', ['id' => $id])->row_array();
    $this->form_validation->set_rules('kriteria', 'Kriteria', 'required');
    $this->form_validation->set_rules('kode', 'Kode', 'required');
    $this->form_validation->set_rules('tipe', 'Tipe', 'required');
    if ($this->form_validation->run() == false) {
      $this->load->view('templates/header', $data);
      $this->load->view('templates/topbar', $data);
      $this->load->view('templates/sidebar', $data);
      $this->load->view('spk/editkriteria', $data);
      $this->load->view('templates/footer', $data);
    } else {
      $this->db->where('id', $this->input->post('id'));
      $this->db->update('spk_kriteria', [
        'kriteria' => $this->input->post('kriteria', true),
        'kode' => $this->input->post('kode', true),
        'tipe' => $this->input->post('tipe', true)
      ]);
      $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">kriteria berhasil diubah.</div>');
      redirect('spk/kriteria');
    }
  }
  public function deleteKriteria($id)
  {
    $this->db->delete('spk_kriteria', ['id' => $id]);
    $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">Kriteria berhasil dihapus.</div>');
    redirect('spk/kriteria');
  }

  public function bobotkriteria()
  {
    $data['title'] = 'Bobot + Kriteria';
    $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
    $data['bobotkriteria'] = $this->db->get('spk_bobot_kriteria')->result_array();
    $this->load->model('Spk_model', 'spk');
    $data['joinbobotkriteria'] = $this->spk->joinBobotKriteria();
    $data['bobot'] = $this->db->get('spk_bobot')->result_array();
    $data['kriteria'] = $this->db->get('spk_kriteria')->result_array();
    $this->load->view('templates/header', $data);
    $this->load->view('templates/topbar', $data);
    $this->load->view('templates/sidebar', $data);
    $this->load->view('spk/bobotkriteria', $data);
    $this->load->view('templates/footer', $data);
  }
  public function tambahBobotKriteria()
  {
    $data['title'] = 'Tambah Bobot + Kriteria';
    $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
    $data['bobot'] = $this->db->get('spk_bobot')->result_array();
    $data['kriteria'] = $this->db->get('spk_kriteria')->result_array();
    $this->form_validation->set_rules('spk_kriteria_id', 'Kriteria', 'required');
    $this->form_validation->set_rules('spk_bobot_id', 'Bobot', 'required');
    if ($this->form_validation->run() == false) {
      $this->load->view('templates/header', $data);
      $this->load->view('templates/topbar', $data);
      $this->load->view('templates/sidebar', $data);
      $this->load->view('spk/tambahbobotkriteria', $data);
      $this->load->view('templates/footer', $data);
    } else {
      $this->db->insert('spk_bobot_kriteria', [
        'spk_kriteria_id' => $this->input->post('spk_kriteria_id', true),
        'spk_bobot_id' => $this->input->post('spk_bobot_id', true)
      ]);
      $this->session->set_flashdata('message', '<div class="alert alert-primary" role="alert">Bobot + Kriteria berhasil ditambah.</div>');
      redirect('spk/bobotkriteria');
    }
  }
  public function editBobotKriteria($id)
  {
    $data['title'] = 'Edit Kriteria';
    $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
    $data['bobotkriteria'] = $this->db->get_where('spk_bobot_kriteria', ['id' => $id])->row_array();
    $data['bobot'] = $this->db->get('spk_bobot')->result_array();
    $data['kriteria'] = $this->db->get('spk_kriteria')->result_array();
    $this->form_validation->set_rules('spk_kriteria_id', 'Kriteria', 'required');
    $this->form_validation->set_rules('spk_bobot_id', 'Bobot', 'required');
    if ($this->form_validation->run() == false) {
      $this->load->view('templates/header', $data);
      $this->load->view('templates/topbar', $data);
      $this->load->view('templates/sidebar', $data);
      $this->load->view('spk/editbobotkriteria', $data);
      $this->load->view('templates/footer', $data);
    } else {
      $this->db->where('id', $this->input->post('id'));
      $this->db->update('spk_bobot_kriteria', [
        'spk_kriteria_id' => $this->input->post('spk_kriteria_id', true),
        'spk_bobot_id' => $this->input->post('spk_bobot_id', true)
      ]);
      $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Bobot + kriteria berhasil diubah.</div>');
      redirect('spk/bobotkriteria');
    }
  }
  public function deleteBobotKriteria($id)
  {
    $this->db->delete('spk_bobot_kriteria', ['id' => $id]);
    $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">Bobot + Kriteria berhasil dihapus.</div>');
    redirect('spk/bobotkriteria');
  }

  public function normalisasi()
  {
    $data['title'] = 'Normalisasi';
    $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
    $data['alternatif'] = $this->db->get('spk_alternatif')->result_array();
    $data['bobot'] = $this->db->get('spk_bobot')->result_array();
    $data['kriteria'] = $this->db->get('spk_kriteria')->result_array();
    $this->load->model('Spk_model', 'spk');
    $data['sumbobot'] = $this->spk->sumPembobotan();
    $data['joinbobotkriteria'] = $this->spk->joinBobotKriteria();
    $this->load->view('templates/header', $data);
    $this->load->view('templates/topbar', $data);
    $this->load->view('templates/sidebar', $data);
    $this->load->view('spk/normalisasi', $data);
    $this->load->view('templates/footer', $data);
  }

  public function inputKriteria()
  {
    $data['title'] = 'Input Kriteria Dari User';
    $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
    $data['kriteria'] = $this->db->get('spk_kriteria')->result_array();
    $data['alternatif'] = $this->db->get('spk_alternatif')->result_array();
    $this->form_validation->set_rules('kriteria_id', 'Kriteria', 'required');
    if ($this->form_validation->run() == false) {
      $this->load->view('templates/header', $data);
      $this->load->view('templates/topbar', $data);
      $this->load->view('templates/sidebar', $data);
      $this->load->view('spk/inputkriteria', $data);
      $this->load->view('templates/footer', $data);
    } else {
      $alternatif_id = $this->input->post('alternatif_id');
      $kriteria_id = $this->input->post('kriteria_id');
      $niai_kriteria = $this->input->post('nilai_kriteria');
      $detail = [
        'alternatif_id' => $alternatif_id,
        'kriteria_id' => $kriteria_id,
        'nilai_kriteria' => $niai_kriteria
      ];
      $this->db->insert('spk_input_kriteria', $detail);
      // $this->db->where('id', $id);
      // $this->db->update('spk_alternatif', [
      //   'is_input' => 1
      // ]);
      $this->session->set_flashdata('message', '<div class="alert alert-primary" role="alert">Berhasil menambah kriteria alternatif.</div>');
      redirect('spk/alternatif');
    }
  }
}
