<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Transactions extends CI_Controller
{
  public function __construct()
  {
    parent::__construct();
    if (!$this->session->userdata('email')) {
      redirect('auth');
    }
    $this->load->model('Transactions_model');
  }

  #transactions
  public function index()
  {
    $data['title'] = 'Transaksi';
    $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
    // $data['transactions'] = $this->db->get('transactions')->result_array();
    $this->load->view('templates/header', $data);
    $this->load->view('templates/sidebar', $data);
    $this->load->view('templates/topbar', $data);
    $this->load->view('transactions/index', $data);
    $this->load->view('templates/footer', $data);
  }
  public function tambahTransactions()
  {
    # code...
  }
  public function editTransactions($id)
  {
    # code...
  }
  public function hapusTransactions($id)
  {
    $this->db->delete('transactions', ['id' => $id]);
    $this->session->set_flashdata('message', 'Berhasil dihapus');
    redirect('transactions/index');
  }

  #stok in
  public function stokIn()
  {
    $data['title'] = 'Stok In';
    $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
    $data['stokin'] = $this->db->get('stokin')->result_array();
    $data['items'] = $this->db->get('items')->result_array();
    $data['supliers'] = $this->db->get('supliers')->result_array();
    $data['joinstokin'] = $this->Transactions_model->joinstokin();
    $this->load->view('templates/header', $data);
    $this->load->view('templates/sidebar', $data);
    $this->load->view('templates/topbar', $data);
    $this->load->view('transactions/stokin', $data);
    $this->load->view('templates/footer', $data);
  }
  public function tambahStokIn()
  {
    $data['title'] = 'Tambah Stok In';
    $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
    $data['items'] = $this->db->get('items')->result_array();
    $data['supliers'] = $this->db->get('supliers')->result_array();
    $this->form_validation->set_rules('items_id', 'Items', 'required');
    if ($this->form_validation->run() == false) {
      $this->load->view('templates/header', $data);
      $this->load->view('templates/sidebar', $data);
      $this->load->view('templates/topbar', $data);
      $this->load->view('transactions/tambahstokin', $data);
      $this->load->view('templates/footer');
    } else {
      $data = [
        'items_id' => $this->input->post('items_id', true),
        'stok_in' => $this->input->post('stok_in', true),
        'supliers_id' => $this->input->post('supliers_id', true),
        'keterangan' => $this->input->post('keterangan', true),
        'created_at' => time()
      ];
      $this->db->insert('stokin', $data);
      $this->session->set_flashdata('message', '<div class="alert alert-primary" role="alert">Berhasil menambah stok in</div>');
      redirect('transactions/stokin');
    }
  }
  public function editStokIn($id)
  {
    $data['title'] = 'Edit Stok In';
    $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
    $data['stokinbyid'] = $this->db->get_where('stokin', ['id' => $id])->row_array();
    $data['supliers'] = $this->db->get('supliers')->result_array();
    $this->form_validation->set_rules('items', 'Items', 'required');
    if ($this->form_validation->run() == false) {
      $this->load->view('templates/header', $data);
      $this->load->view('templates/sidebar', $data);
      $this->load->view('templates/topbar', $data);
      $this->load->view('transactions/editstokin', $data);
      $this->load->view('templates/footer');
    } else {
      $this->db->where('id', $this->input->post('id'));
      $this->db->update('stokin', [
        'items' => $this->input->post('items', true),
        'stok' => $this->input->post('stok', true),
        'supliers_id' => $this->input->post('supliers_id', true),
        'keterangan' => $this->input->post('keterangan', true)
      ]);
    }
    $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Berhasil mengubah stok in</div>');
    redirect('transactions/stokin');
  }
  public function hapusStokIn($id)
  {
    $this->db->delete('stokin', ['id' => $id]);
    $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">Berhasil mengahpus stok in</div>');
    redirect('transactions/stokin');
  }

  #stok out
  public function stokOut()
  {
    # code...
  }
  public function tambahStokOut()
  {
    # code...
  }
  public function editStokOut($id)
  {
    # code...
  }
  public function hapusStokOut($id)
  {
    # code...
  }
}
