<?php
defined('BASEPATH') or exit('No direct script access allowed');

class User extends CI_Controller
{
  public function __construct()
  {
    parent::__construct();
    if (!$this->session->userdata('email')) {
      redirect('auth');
    }
    $this->load->model('User_model');
  }
  public function index()
  {
    $data['title'] = 'Profile';
    $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
    $data['roleketerangan'] = $this->User_model->roleketerangan();
    $this->load->view('templates/header', $data);
    $this->load->view('templates/topbar', $data);
    $this->load->view('templates/sidebar', $data);
    $this->load->view('user/index', $data);
    $this->load->view('templates/footer', $data);
  }

  public function ubahDataUser()
  {
    $data['title'] = 'Ubah Profile';
    $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();

    $this->form_validation->set_rules('name', 'Name', 'required');

    if ($this->form_validation->run() == false) {
      $this->load->view('templates/header', $data);
      $this->load->view('templates/sidebar', $data);
      $this->load->view('templates/topbar', $data);
      $this->load->view('user/ubahdatauser', $data);
      $this->load->view('templates/footer', $data);
    } else {
      $name = $this->input->post('name');
      $email = $this->input->post('email');
      $upload_image = $_FILES['image']['name'];
      if ($upload_image) {
        $config['allowed_types'] = 'gif|jpg|png';
        $config['max_size'] = '2048';
        $config['upload_path'] = './assets/dist/img/profile/';
        $this->load->library('upload', $config);
        if ($this->upload->do_upload('image')) {
          $old_image = $data['user']['name'];
          if ($old_image != 'default.jpg') {
            unlink(FCPATH . 'assets/dist/img/profile/' . $old_image);
          }
          $new_image = $this->upload->data('file_name');
          $this->db->set('image', $new_image);
        } else {
          echo $this->upload->display_errors();
        }
      }
      $this->db->set('name', $name);
      $this->db->where('email', $email);
      $this->db->update('user');
      $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Profile berhasil diubah.</div>');
      redirect('user');
    }
  }

  public function lamarankerja()
  {
    // 
  }
  public function tambahlamarankerja()
  {
    $data['title'] = 'Tambah Lamaran Kerja';
    $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
    $config['upload_path']          = './uploads/';
    $config['allowed_types']        = 'pdf';
    $config['max_size']             = 0;
    // $config['max_width']            = 1024;
    // $config['max_height']           = 768;
    $this->load->library('upload', $config);
    if (!$this->upload->do_upload('upload_berkas')) {
      $this->form_validation->set_rules('upload_berkas', 'Upload Berkas', 'required');
      $this->load->view('templates/header', $data);
      $this->load->view('templates/topbar', $data);
      $this->load->view('templates/sidebar', $data);
      $this->load->view('user/tambahlamarankerja', $data);
      $this->load->view('templates/footer', $data);
    } else {
      $upload_data = $this->upload->data();
      $data = [
        'user_id' => $this->input->post('user_id'),
        'upload_berkas' => $upload_data['file_name'],
        'is_input' => $this->input->post('is_input')
      ];
      $this->db->insert('lamaran_kerja', $data);
      $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Berhasil upload lamaran kerja.</div>');
      redirect('user/index');
    }
  }
}
