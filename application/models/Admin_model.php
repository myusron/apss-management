<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Admin_model extends CI_Model
{
  public function ambilData()
  {
    # code...
  }

  public function tambahData()
  {
    # code...
  }

  public function hapusData($id)
  {
    # code...
  }

  public function ambilDataById($id)
  {
    # code...
  }

  public function ubahData()
  {
    # code...
  }

  public function listuserdetailid()
  {
    //
  }

  public function ambilDataRole()
  {
    return $this->db->get('user_role')->result_array();
  }

  public function tambahDataRole()
  {
    $data = [
      'role' => $this->input->post('role', true)
    ];
    $this->db->insert('user_role', $data);
  }

  public function hapusDataRole($id)
  {
    $this->db->delete('user_role', ['id' => $id]);
  }

  public function ambilDataByIdRole($id)
  {
    return $this->db->get_where('user_role', ['id' => $id])->row_array();
  }

  public function ubahDataRole()
  {
    $data = [
      'role' => $this->input->post('role')
    ];
    $this->db->where('id', $this->input->post('id'));
    $this->db->update('user_role', $data);
  }

  public function tambahDataListUser()
  {
    $data = [
      'name' => htmlspecialchars($this->input->post('name', true)),
      'email' => htmlspecialchars($this->input->post('email', true)),
      'image' => 'default.jpg',
      'password' => password_hash($this->input->post('password'), PASSWORD_DEFAULT),
      'role_id' => 2,
      'is_active' => 1,
      'date_created' => time()
    ];
    $this->db->insert('user', $data);
  }

  public function getListUser()
  {
    return $this->db->get('user')->result_array();
  }

  public function getDataUserById($id)
  {
    return $this->db->get_where('user', ['id' => $id])->row_array();
  }

  public function countDataUser()
  {
    $countDataUser = "select count(`id`) as jumlahuser from `user`";
    return $this->db->query($countDataUser)->result();
  }

  public function countDataCategories()
  {
    $countDataCategories = "select count(`id`) as jumlahcategories from `categories`";
    return $this->db->query($countDataCategories)->result();
  }

  // supliers
  public function countDataSupliers()
  {
    $countDataSupliers = "select count(`id`) as jumlahsupliers from `supliers`";
    return $this->db->query($countDataSupliers)->result();
  }

  // units
  public function countDataUnits()
  {
    $countDataUnits = "select count(`id`) as jumlahunits from `units`";
    return $this->db->query($countDataUnits)->result();
  }

  // items
  public function countDataItems()
  {
    $countDataItems = "select count(`id`) as jumlahitems from `items`";
    return $this->db->query($countDataItems)->result();
  }

  public function countdataisinput()
  {
    $query = "select count(`is_input`) as is_input from `lamaran_kerja` where `is_input` = 0";
    return $this->db->query($query)->result();
  }

  public function listjoin()
  {
    $query = "select
    `a`.*,
    `b`.`name`
    from `lamaran_kerja` `a`
    join `user` `b` on `a`.`user_id`=`b`.`id`
    where `is_input` = 1";
    return $this->db->query($query)->result_array();
  }

  public function getAllPelamar()
  {
    $query = "select
    a.*,
    b.name
    from lamaran_kerja a
    join user b on a.user_id=b.id
    ";
    return $this->db->query($query)->result_array();
  }
}
