<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Master_model extends CI_Model
{
  public function getDataById($id)
  {
    return $this->db->get_where('master', ['id' => $id])->row_array();
  }

  public function ubahData()
  {
    $data = [
      'name' => $this->input->post('name'),
      'phone' => $this->input->post('phone'),
      'email' => $this->input->post('email'),
      'address' => $this->input->post('address')
    ];

    $this->db->where('id', $this->input->post('id'));
    $this->db->update('master', $data);
  }

  public function hitungIdBaru()
  {
    $idbaru = "select `id` from `supliers` where `id` in(select max(`id`) from `supliers`)";
    return $this->db->query($idbaru)->row_array();
  }
}
