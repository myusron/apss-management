<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Menu_model extends CI_Model
{
  public function getSubMenu()
  {
    $query = "select `user_sub_menu`.*, `user_menu`.`menu`
    from `user_sub_menu` join `user_menu`
    on `user_sub_menu`.`menu_id` = `user_menu`.`id`";
    return $this->db->query($query)->result_array();
  }

  public function ambilDataMenuByI($id)
  {
    // return db get_where user_menu, id, row_array
    return $this->db->get_where('user_menu', ['id' => $id])->row_array();
  }

  public function ubahDataMenu()
  {
    // siapin data di array [data diambil dari inpit post]
    $data = [
      'menu' => $this->input->post('menu'),
      'icon' => $this->input->post('icon')
    ];
    // update where id
    $this->db->where('id', $this->input->post('id'));
    $this->db->update('user_menu', $data);
  }

  public function submenuTambah()
  {
    $data = [
      'title' => $this->input->post('menu', true),
      'menu_id' => $this->input->post('menu_id', true),
      'url' => $this->input->post('url', true),
      'iconsubmenu' => $this->input->post('iconsubmenu', true),
      'is_active' => $this->input->post('is_active', true)
    ];
    $this->db->insert('user_sub_menu', $data);
  }

  public function submenuById($id)
  {
    return $this->db->get_where('user_sub_menu', ['id' => $id])->row_array();
  }

  public function editSubmenu()
  {
    $data = [
      'menu_id' => $this->input->post('menu_id'),
      'title' => $this->input->post('menu'),
      'url' => $this->input->post('url'),
      'iconsubmenu' => $this->input->post('iconsubmenu'),
      'is_active' => $this->input->post('is_active')
    ];
    $this->db->where('id', $this->input->post('id'));
    $this->db->update('user_sub_menu', $data);
  }
}
