<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Products_model extends CI_Model
{
  public function insertDataCategories()
  {
    $data = [
      'categories' => $this->input->post('categories', true),
      'codecategories' => $this->input->post('codecategories', true)
    ];
    $this->db->insert('categories', $data);
  }

  public function hitungIdBaru()
  {
    $idbaru = "select `id` from `categories` where `id` in(select max(`id`) from `categories`)";
    return $this->db->query($idbaru)->row_array();
  }

  public function hitungiditems()
  {
    $idbaru = "select `id` from `items` where `id` in(select max(`id`) from `items`)";
    return $this->db->query($idbaru)->row_array();
  }

  public function getCategoriesById($id)
  {
    return $this->db->get_where('categories', ['id' => $id])->row_array();
  }

  public function ubahDataCategories()
  {
    $data = [
      'codecategories' => $this->input->post('codecategories', true),
      'categories' => $this->input->post('categories', true)
    ];
    $this->db->where('id', $this->input->post('id'));
    $this->db->update('categories', $data);
  }

  public function joinDataItems()
  {
    $joinitem = "select
    `items`.*, `supliers`.`nama_supliers`, `categories`.`categories`, `units`.`units`
    from
    `items`
    join `supliers` on `supliers`.`id`=`items`.`supliers_id`
    join `categories` on `categories`.`id`=`items`.`categories_id`
    join `units` on `units`.`id`=`items`.`units_id`
    ";
    return $this->db->query($joinitem)->result_array();
  }
}
