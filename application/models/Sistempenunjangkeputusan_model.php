<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Sistempenunjangkeputusan_model extends CI_Model
{
  public function joinKriteriaUser()
  {
    $query = "select
              `b`.`name`, `a`.*
              from `kriteria_kandidat` `a`
              join `user` `b` on `a`.`user_id`=`b`.`id`";
    return $this->db->query($query)->result_array();
  }

  public function joinPembobotanKriteria()
  {
    $query = "select
    `a`.*,
    `b`.`kriteria`,
    `b`.`tipe`,
    `c`.`bobot` as `preferensi`,
    `c`.`kepentingan` as `keterangan`
    from `pembobotan` `a`
    join `tipe_kriteria` `b` on `b`.`id`=`a`.`tipe_kriteria_id`
    join `kepentingan_kriteria` `c` on `c`.`id`=`a`.`kepentingan_kriteria_id`
    order by `a`.`tipe_kriteria_id`";
    return $this->db->query($query)->result_array();
  }

  public function countPembobotan()
  {
    $query = "select
    sum(kepentingan_kriteria_id) as sumkriteria
     from `pembobotan` `a`
     join `tipe_kriteria` `b` on `b`.`id`=`a`.`tipe_kriteria_id`
     join `kepentingan_kriteria` `c` on `c`.`id`=`a`.`kepentingan_kriteria_id`
     order by `a`.`tipe_kriteria_id`";
    return $this->db->query($query)->result();
  }

  public function pelamar()
  {
    $query = "select 
    *
    from
    `user`
    where `role_id` = 9";
    return $this->db->query($query)->result_array();
  }

  public function kepentinganKriteria()
  {
    $query = "select
    `kepentingan_kriteria_id` as kepentingan_kriteria
    from
    `pembobotan` 
    order by `tipe_kriteria_id`";
    return $this->db->query($query)->result();
  }

  public function kepentinganKriteriaId1()
  {
    $query = "
    select
    `kepentingan_kriteria_id` as kepentingan_kriteria1
    from
    `pembobotan` 
    where `tipe_kriteria_id` = 1";
    return $this->db->query($query)->result();
  }
  public function kepentinganKriteriaId2()
  {
    $query = "
    select
    `kepentingan_kriteria_id` as kepentingan_kriteria2
    from
    `pembobotan` 
    where `tipe_kriteria_id` = 2";
    return $this->db->query($query)->result();
  }
  public function kepentinganKriteriaId3()
  {
    $query = "
    select
    `kepentingan_kriteria_id` as kepentingan_kriteria3
    from
    `pembobotan` 
    where `tipe_kriteria_id` = 3";
    return $this->db->query($query)->result();
  }
  public function kepentinganKriteriaId4()
  {
    $query = "
    select
    `kepentingan_kriteria_id` as kepentingan_kriteria4
    from
    `pembobotan` 
    where `tipe_kriteria_id` = 4";
    return $this->db->query($query)->result();
  }
  public function kepentinganKriteriaId5()
  {
    $query = "
    select
    `kepentingan_kriteria_id` as kepentingan_kriteria5
    from
    `pembobotan` 
    where `tipe_kriteria_id` = 5";
    return $this->db->query($query)->result();
  }
  public function kepentinganKriteriaId6()
  {
    $query = "
    select
    `kepentingan_kriteria_id` as kepentingan_kriteria6
    from
    `pembobotan` 
    where `tipe_kriteria_id` = 6";
    return $this->db->query($query)->result();
  }
  public function kepentinganKriteriaId7()
  {
    $query = "
    select
    `kepentingan_kriteria_id` as kepentingan_kriteria7
    from
    `pembobotan` 
    where `tipe_kriteria_id` = 7";
    return $this->db->query($query)->result();
  }
  public function kepentinganKriteriaId8()
  {
    $query = "
    select
    `kepentingan_kriteria_id` as kepentingan_kriteria8
    from
    `pembobotan` 
    where `tipe_kriteria_id` = 8";
    return $this->db->query($query)->result();
  }

  public function C1id5()
  {
    $query = "select `C1` as `c1` from `lamaran_kerja` where `id` = 5";
    return $this->db->query($query)->result();
  }
  public function C2id5()
  {
    $query = "select `C2` as `c2` from `lamaran_kerja` where `id` = 5";
    return $this->db->query($query)->result();
  }
  public function C3id5()
  {
    $query = "select `C3` as `c3` from `lamaran_kerja` where `id` = 5";
    return $this->db->query($query)->result();
  }
  public function C4id5()
  {
    $query = "select `C4` as `c4` from `lamaran_kerja` where `id` = 5";
    return $this->db->query($query)->result();
  }
  public function C5id5()
  {
    $query = "select `C5` as `c5` from `lamaran_kerja` where `id` = 5";
    return $this->db->query($query)->result();
  }
  public function C6id5()
  {
    $query = "select `C6` as `c6` from `lamaran_kerja` where `id` = 5";
    return $this->db->query($query)->result();
  }
  public function C7id5()
  {
    $query = "select `C7` as `c7` from `lamaran_kerja` where `id` = 5";
    return $this->db->query($query)->result();
  }
  public function C8id5()
  {
    $query = "select `C8` as `c8` from `lamaran_kerja` where `id` = 5";
    return $this->db->query($query)->result();
  }

  public function C1id6()
  {
    $query = "select `C1` as `c1` from `lamaran_kerja` where `id` = 6";
    return $this->db->query($query)->result();
  }
  public function C2id6()
  {
    $query = "select `C2` as `c2` from `lamaran_kerja` where `id` = 6";
    return $this->db->query($query)->result();
  }
  public function C3id6()
  {
    $query = "select `C3` as `c3` from `lamaran_kerja` where `id` = 6";
    return $this->db->query($query)->result();
  }
  public function C4id6()
  {
    $query = "select `C4` as `c4` from `lamaran_kerja` where `id` = 6";
    return $this->db->query($query)->result();
  }
  public function C5id6()
  {
    $query = "select `C5` as `c5` from `lamaran_kerja` where `id` = 6";
    return $this->db->query($query)->result();
  }
  public function C6id6()
  {
    $query = "select `C6` as `c6` from `lamaran_kerja` where `id` = 6";
    return $this->db->query($query)->result();
  }
  public function C7id6()
  {
    $query = "select `C7` as `c7` from `lamaran_kerja` where `id` = 6";
    return $this->db->query($query)->result();
  }
  public function C8id6()
  {
    $query = "select `C8` as `c8` from `lamaran_kerja` where `id` = 6";
    return $this->db->query($query)->result();
  }

  public function C1id7()
  {
    $query = "select `C1` as `c1` from `lamaran_kerja` where `id` = 7";
    return $this->db->query($query)->result();
  }
  public function C2id7()
  {
    $query = "select `C2` as `c2` from `lamaran_kerja` where `id` = 7";
    return $this->db->query($query)->result();
  }
  public function C3id7()
  {
    $query = "select `C3` as `c3` from `lamaran_kerja` where `id` = 7";
    return $this->db->query($query)->result();
  }
  public function C4id7()
  {
    $query = "select `C4` as `c4` from `lamaran_kerja` where `id` = 7";
    return $this->db->query($query)->result();
  }
  public function C5id7()
  {
    $query = "select `C5` as `c5` from `lamaran_kerja` where `id` = 7";
    return $this->db->query($query)->result();
  }
  public function C6id7()
  {
    $query = "select `C6` as `c6` from `lamaran_kerja` where `id` = 7";
    return $this->db->query($query)->result();
  }
  public function C7id7()
  {
    $query = "select `C7` as `c7` from `lamaran_kerja` where `id` = 7";
    return $this->db->query($query)->result();
  }
  public function C8id7()
  {
    $query = "select `C8` as `c8` from `lamaran_kerja` where `id` = 7";
    return $this->db->query($query)->result();
  }

  public function C1id8()
  {
    $query = "select `C1` as `c1` from `lamaran_kerja` where `id` = 8";
    return $this->db->query($query)->result();
  }
  public function C2id8()
  {
    $query = "select `C2` as `c2` from `lamaran_kerja` where `id` = 8";
    return $this->db->query($query)->result();
  }
  public function C3id8()
  {
    $query = "select `C3` as `c3` from `lamaran_kerja` where `id` = 8";
    return $this->db->query($query)->result();
  }
  public function C4id8()
  {
    $query = "select `C4` as `c4` from `lamaran_kerja` where `id` = 8";
    return $this->db->query($query)->result();
  }
  public function C5id8()
  {
    $query = "select `C5` as `c5` from `lamaran_kerja` where `id` = 8";
    return $this->db->query($query)->result();
  }
  public function C6id8()
  {
    $query = "select `C6` as `c6` from `lamaran_kerja` where `id` = 8";
    return $this->db->query($query)->result();
  }
  public function C7id8()
  {
    $query = "select `C7` as `c7` from `lamaran_kerja` where `id` = 8";
    return $this->db->query($query)->result();
  }
  public function C8id8()
  {
    $query = "select `C8` as `c8` from `lamaran_kerja` where `id` = 8";
    return $this->db->query($query)->result();
  }

  public function C1id9()
  {
    $query = "select `C1` as `c1` from `lamaran_kerja` where `id` = 9";
    return $this->db->query($query)->result();
  }
  public function C2id9()
  {
    $query = "select `C2` as `c2` from `lamaran_kerja` where `id` = 9";
    return $this->db->query($query)->result();
  }
  public function C3id9()
  {
    $query = "select `C3` as `c3` from `lamaran_kerja` where `id` = 9";
    return $this->db->query($query)->result();
  }
  public function C4id9()
  {
    $query = "select `C4` as `c4` from `lamaran_kerja` where `id` = 9";
    return $this->db->query($query)->result();
  }
  public function C5id9()
  {
    $query = "select `C5` as `c5` from `lamaran_kerja` where `id` = 9";
    return $this->db->query($query)->result();
  }
  public function C6id9()
  {
    $query = "select `C6` as `c6` from `lamaran_kerja` where `id` = 9";
    return $this->db->query($query)->result();
  }
  public function C7id9()
  {
    $query = "select `C7` as `c7` from `lamaran_kerja` where `id` = 9";
    return $this->db->query($query)->result();
  }
  public function C8id9()
  {
    $query = "select `C8` as `c8` from `lamaran_kerja` where `id` = 9";
    return $this->db->query($query)->result();
  }

  public function C1id10()
  {
    $query = "select `C1` as `c1` from `lamaran_kerja` where `id` = 10";
    return $this->db->query($query)->result();
  }
  public function C2id10()
  {
    $query = "select `C2` as `c2` from `lamaran_kerja` where `id` = 10";
    return $this->db->query($query)->result();
  }
  public function C3id10()
  {
    $query = "select `C3` as `c3` from `lamaran_kerja` where `id` = 10";
    return $this->db->query($query)->result();
  }
  public function C4id10()
  {
    $query = "select `C4` as `c4` from `lamaran_kerja` where `id` = 10";
    return $this->db->query($query)->result();
  }
  public function C5id10()
  {
    $query = "select `C5` as `c5` from `lamaran_kerja` where `id` = 10";
    return $this->db->query($query)->result();
  }
  public function C6id10()
  {
    $query = "select `C6` as `c6` from `lamaran_kerja` where `id` = 10";
    return $this->db->query($query)->result();
  }
  public function C7id10()
  {
    $query = "select `C7` as `c7` from `lamaran_kerja` where `id` = 10";
    return $this->db->query($query)->result();
  }
  public function C8id10()
  {
    $query = "select `C8` as `c8` from `lamaran_kerja` where `id` = 10";
    return $this->db->query($query)->result();
  }

  public function C1id11()
  {
    $query = "select `C1` as `c1` from `lamaran_kerja` where `id` = 11";
    return $this->db->query($query)->result();
  }
  public function C2id11()
  {
    $query = "select `C2` as `c2` from `lamaran_kerja` where `id` = 11";
    return $this->db->query($query)->result();
  }
  public function C3id11()
  {
    $query = "select `C3` as `c3` from `lamaran_kerja` where `id` = 11";
    return $this->db->query($query)->result();
  }
  public function C4id11()
  {
    $query = "select `C4` as `c4` from `lamaran_kerja` where `id` = 11";
    return $this->db->query($query)->result();
  }
  public function C5id11()
  {
    $query = "select `C5` as `c5` from `lamaran_kerja` where `id` = 11";
    return $this->db->query($query)->result();
  }
  public function C6id11()
  {
    $query = "select `C6` as `c6` from `lamaran_kerja` where `id` = 11";
    return $this->db->query($query)->result();
  }
  public function C7id11()
  {
    $query = "select `C7` as `c7` from `lamaran_kerja` where `id` = 11";
    return $this->db->query($query)->result();
  }
  public function C8id11()
  {
    $query = "select `C8` as `c8` from `lamaran_kerja` where `id` = 11";
    return $this->db->query($query)->result();
  }
}
