<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Spk_model extends CI_Model
{
  public function joinBobotKriteria()
  {
    $query = "select 
    a.id,
    b.kriteria as kriteria,
    b.kode as kode,
    c.bobot as bobot,
    c.kepentingan as kepentingan
    from spk_bobot_kriteria a
    join spk_kriteria b on a.spk_kriteria_id = b.id
    join spk_bobot c on a.spk_bobot_id = c.id
    order by b.id";
    return $this->db->query($query)->result_array();
  }

  public function sumPembobotan()
  {
    $query = "select
    sum(b.bobot) as bobot
    from
    spk_bobot_kriteria a
    join spk_bobot b on a.spk_bobot_id=b.id
    join spk_kriteria c on a.spk_kriteria_id=c.id";
    return $this->db->query($query)->result();
  }

  public function countKriteria()
  {
    $query = "select 
    count(id) 
    from
    spk_kriteria";
    return $this->db->query($query)->result();
  }
}
