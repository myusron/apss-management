<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Transactions_model extends CI_Model
{
  public function joinstokin()
  {
    $query = "select
    a.*,
    b.id, b.kode_items, b.nama_items as nama_items,
    c.id, c.kode_supliers, c.nama_supliers as nama_supliers
    from
    stokin a
    join items b on a.items_id=b.id
    join supliers c on a.supliers_id=c.id";
    return $this->db->query($query)->result_array();
  }
}
