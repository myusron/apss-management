<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <?php
  $id = $listuserdetail['role_id'];
  $query = "select `user_role`.`role`, `user`.`name`
  from `user_role`
  join `user`
  on `user`.`role_id`=`user_role`.`id`
  where `user`.`role_id` = $id
  ";
  $result = $this->db->query($query)->row_array();

  ?>
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1><?= $title; ?></h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active"><?= $title; ?></li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-3">
          <?= $this->session->flashdata('message'); ?>

          <!-- Profile Image -->
          <div class="card card-primary card-outline">
            <div class="card-body box-profile">
              <div class="text-center">
                <img class="profile-user-img img-fluid img-circle" src="<?= base_url('assets/dist/img/profile/') . $listuserdetail['image']; ?>" alt="User profile picture">
              </div>

              <h3 class="profile-username text-center"><?= $listuserdetail['name']; ?></h3>

              <p class="text-muted text-center"><?= $listuserdetail['email']; ?></p>

              <ul class="list-group list-group-unbordered mb-3 text-center">
                <li class="list-group-item text-center">
                  <b>Role : </b>
                  <a class="">
                    <?= $result['role']; ?>
                  </a>
                </li>
                <li class="list-group-item text-center">
                  <b>Active since : </b> <a class=""><?= date('d F Y', $listuserdetail['date_created']); ?></a>
                </li>
              </ul>

              <a href="<?= base_url('admin/listUserUbah/') . $listuserdetail['id']; ?>" class="btn btn-primary btn-block">Ubah</a>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </div><!-- /.container-fluid -->
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->