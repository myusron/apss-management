<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <?php
  $id = $this->session->userdata('role_id');
  $query = "select `user`.`name`, `user`.`role_id`, `user_role`.`role`
  from `user`
  join `user_role`
  on `user`.`role_id`=`user_role`.`id`
  where `user`.`id` = $id
  ";
  $result = $this->db->query($query)->row_array();

  ?>
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1><?= $title; ?></h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active"><?= $title; ?></li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-3">
          <?= $this->session->flashdata('message'); ?>

          <?= form_open_multipart('admin/listUserUbah/' . $listuserdetail['id']); ?>
          <input type="hidden" id="id" name="id" value="<?= $listuserdetail['id']; ?>">
          <input type="hidden" id="password" name="password" value="<?= $listuserdetail['password']; ?>">
          <input type="hidden" id="is_active" name="is_active" value="<?= $listuserdetail['is_active']; ?>">
          <input type="hidden" id="date_created" name="date_created" value="<?= $listuserdetail['date_created']; ?>">
          <!-- Profile Image -->
          <div class="card card-primary card-outline">
            <div class="card-body box-profile">
              <div class="text-center">
                <img class="profile-user-img img-fluid img-circle" src="<?= base_url('assets/dist/img/profile/') . $listuserdetail['image']; ?>" alt="User profile picture">
              </div>
              <div class="custom-file text-center">
                <label for="image" class="text-center">pilih gambar</label>
                <input type="file" class="" id="image" name="image">
              </div>
              <hr>

              <div class="custome-file">
                <label for="name">Nama :</label>
                <input type="text" id="name" name="name" value="<?= $listuserdetail['name']; ?>">
                <small><?= form_error('name'); ?></small>
              </div>
              <div class="custome-file">
                <label for="email">Email :</label>
                <input type="text" id="email" name="email" value="<?= $listuserdetail['email']; ?>">
                <small><?= form_error('email'); ?></small>
              </div>
              <div class="custome-file">
                <label for="role_id">pilih role</label>
                <select name="role_id" id="role_id">
                  <?php foreach ($role as $r) : ?>
                    <?php if ($r['id'] == $listuserdetail['role_id']) : ?>
                      <option value="<?= $r['id']; ?>" selected><?= $r['role']; ?></option>
                    <?php else : ?>
                      <option value="<?= $r['id']; ?>"><?= $r['role']; ?></option>
                    <?php endif; ?>
                  <?php endforeach; ?>
                </select>
              </div>

              <ul class="list-group list-group-unbordered mb-3 text-center">
                <li class="list-group-item text-center">
                  <b>Active since : </b> <a class=""><?= date('d F Y', $listuserdetail['date_created']); ?></a>
                </li>
              </ul>

              <button type="submit">ubah</button>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </div><!-- /.container-fluid -->
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->