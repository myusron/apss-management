<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1><?= $title; ?></h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active"><?= $title; ?></li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-6">
          <div class="card card-primary">
            <div class="card-header">
            </div>
            <!-- /.card-header -->
            <!-- form start -->
            <form action="" method="post">
              <div class="card-body">
                <div class="form-group">
                  <input type="hidden" name="id" id="id" value="<?= $detailcustomers['id']; ?>">
                  <label for="nama">Nama Customers</label>
                  <input type="text" class="form-control" id="nama" name="nama" placeholder="" value="<?= $detailcustomers['nama']; ?>">
                  <small><?= form_error('nama'); ?></small>
                </div>
                <div class="form-group">
                  <label for="jenis_kelamin">Jenis Kelamin</label>
                  <select name="jenis_kelamin" id="jenis_kelamin" class="form-control">
                    <option value="<?= $detailcustomers['jenis_kelamin']; ?>"><?= $detailcustomers['jenis_kelamin']; ?></option>
                    <option value="Laki-laki">Laki-laki</option>
                    <option value="Perempuan">Perempuan</option>
                  </select>
                  <small><?= form_error('jenis_kelamin'); ?></small>
                </div>
                <div class="form-group">
                  <label for="no_telpon">No Telpon</label>
                  <input type="text" class="form-control" id="no_telpon" name="no_telpon" placeholder="" value="<?= $detailcustomers['no_telpon']; ?>">
                  <small><?= form_error('no_telpon'); ?></small>
                </div>
                <div class="form-group">
                  <label for="alamat">Alamat</label>
                  <input type="text" class="form-control" id="alamat" name="alamat" placeholder="" value="<?= $detailcustomers['alamat']; ?>">
                  <small><?= form_error('alamat'); ?></small>
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary" name="">Simpan</button>
                  <a href="<?= base_url('master/customers'); ?>" class="btn btn-danger" name="">Cancel</a>
                </div>
            </form>
          </div>
          <!-- /.card -->
        </div>
      </div>
    </div>

  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->