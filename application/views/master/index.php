<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1><?= $title; ?></h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active"><?= $title; ?></li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <!-- Main content -->
  <section class="content">

    <div class="card">
      <div class="card-header">
        <?= $this->session->flashdata('message'); ?>
        <h3 class="card-title">
          <a href="<?= base_url('master/tambahdata'); ?>" class="btn btn-primary">Tambah Data</a>
        </h3>
      </div>
      <!-- /.card-header -->
      <div class="card-body">
        <table class="table table-bordered" id="example1">
          <thead>
            <tr>
              <th style="width: 10px">#</th>
              <th>Name</th>
              <th>Phone</th>
              <th>Email</th>
              <th>Address</th>
              <th style="width: 120px">Action </th>
            </tr>
          </thead>
          <tbody>
            <?php $i = 1; ?>
            <?php foreach ($master as $m) : ?>
              <tr>
                <td><?= $i++; ?></td>
                <td><?= $m['name']; ?></td>
                <td><?= $m['phone']; ?></td>
                <td><?= $m['email']; ?></td>
                <td><?= $m['address']; ?></td>
                <td>
                  <span class="badge bg-success"><a href="<?= base_url('master/ubah/') . $m['id']; ?>">edit</a></span>
                  <span class="badge bg-danger"><a href="<?= base_url('master/delete/') . $m['id']; ?>" onclick="return confirm('apakah akan dihapus?')">delete</a></span>
                </td>
              </tr>
            <?php endforeach; ?>
          </tbody>
        </table>
      </div>
      <!-- /.card-body -->
    </div>
    <!-- /.card -->

  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->