<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1><?= $title; ?></h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active"><?= $title; ?></li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <!-- Main content -->
  <section class="content">

    <div class="card">
      <?= $this->session->flashdata('message'); ?>
      <div class="card-header">
        <h3 class="card-title">
          <a href="<?= base_url('master/tambahsupliers'); ?>" class="btn btn-primary">Tambah Supliers</a>
        </h3>
      </div>
      <!-- /.card-header -->
      <div class="card-body">
        <table class="table table-bordered" id="example1">
          <thead>
            <tr>
              <th style="width: 10px">#</th>
              <th>Kode Supliers</th>
              <th>Nama Supliers</th>
              <th>Alamat Supliers</th>
              <th>No Telpon</th>
              <th style="width: 120px">Action </th>
            </tr>
          </thead>
          <tbody>
            <?php $i = 1; ?>
            <?php foreach ($supliers as $su) : ?>
              <tr>
                <td><?= $i++; ?></td>
                <td><?= $su['kode_supliers']; ?></td>
                <td><?= $su['nama_supliers']; ?></td>
                <td><?= $su['alamat_supliers']; ?></td>
                <td><?= $su['no_telpon']; ?></td>
                <td>
                  <span class="badge bg-success"><a href="<?= base_url('master/editsupliers/') . $su['id']; ?>">edit</a></span>
                  <span class="badge bg-danger"><a href="<?= base_url('master/hapusSupliers/') . $su['id']; ?>" onclick="return confirm('apakah akan dihapus?')">delete</a></span>
                </td>
              </tr>
            <?php endforeach; ?>
          </tbody>
        </table>
      </div>
      <!-- /.card-body -->
    </div>
    <!-- /.card -->

  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->