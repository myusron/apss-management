<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1><?= $title; ?></h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active"><?= $title; ?></li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-6">
          <div class="card card-primary">
            <div class="card-header">
            </div>
            <!-- /.card-header -->
            <!-- form start -->
            <form action="" method="post">
              <div class="card-body">
                <?php $i = $idbaru['id'] + 1; ?>
                <div class="form-group">
                  <label for="kode_supliers">Kode Supliers</label>
                  <input type="text" class="form-control" id="kode_supliers" name="kode_supliers" placeholder="" readonly value="SPL00<?= $i; ?>">
                  <small><?= form_error('kode_supliers'); ?></small>
                </div>
                <div class="form-group">
                  <label for="nama_supliers">Nama Supliers</label>
                  <input type="text" class="form-control" id="nama_supliers" name="nama_supliers" placeholder="">
                  <small><?= form_error('nama_supliers'); ?></small>
                </div>
                <div class="form-group">
                  <label for="alamat_supliers">Alamat Supliers</label>
                  <input type="text" class="form-control" id="alamat_supliers" name="alamat_supliers" placeholder="">
                  <small><?= form_error('alamat_supliers'); ?></small>
                </div>
                <div class="form-group">
                  <label for="no_telpon">No Telpon</label>
                  <input type="text" class="form-control" id="no_telpon" name="no_telpon" placeholder="">
                  <small><?= form_error('no_telpon'); ?></small>
                </div>
                <input type="hidden" name="is_active" id="is_active">
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary" name="">Simpan</button>
                  <a href="<?= base_url('master/supliers'); ?>" class="btn btn-danger" name="">Cancel</a>
                </div>
            </form>
          </div>
          <!-- /.card -->
        </div>
      </div>
    </div>

  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->