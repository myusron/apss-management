<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1><?= $title; ?></h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active"><?= $title; ?></li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-6">
          <div class="card card-primary">
            <div class="card-header">
              <h3 class="card-title"><?= $master['name']; ?></h3>
            </div>
            <!-- /.card-header -->
            <!-- form start -->
            <form action="" method="post">
              <div class="card-body">
                <input type="hidden" name="id" id="id" value="<?= $master['id']; ?>">
                <div class="form-group">
                  <input type="text" class="form-control" id="name" name="name" value="<?= $master['name']; ?>">
                  <small><?= form_error('name'); ?></small>
                </div>
                <div class="form-group">
                  <input type="text" class="form-control" id="phone" name="phone" value="<?= $master['phone']; ?>">
                  <small><?= form_error('phone'); ?></small>
                </div>
                <div class="form-group">
                  <input type="text" class="form-control" id="email" name="email" value="<?= $master['email']; ?>">
                  <small><?= form_error('email'); ?></small>
                </div>
                <div class="form-group">
                  <input type="text" class="form-control" id="address" name="address" value="<?= $master['address']; ?>">
                  <small><?= form_error('address'); ?></small>
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary" name="ubah">Submit</button>
                </div>
            </form>
          </div>
          <!-- /.card -->
        </div>
      </div>
    </div>

  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->