<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1><?= $title; ?></h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active"><?= $title; ?></li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-8">
          <div class="card card-primary">
            <div class="card-header">
            </div>
            <!-- /.card-header -->
            <!-- form start -->
            <form action="" method="post">
              <div class="card-body">
                <div class="row">
                  <!-- select menu -->
                  <div class="form-group col-md-6">
                    <label for="menu_id">Menu Utama</label><br>
                    <select name="menu_id" id="menu_id">
                      <option value="">Pilih Menu Utama</option>
                      <?php foreach ($menu as $m) : ?>
                        <option value="<?= $m['id']; ?>"><?= $m['menu']; ?></option>
                      <?php endforeach; ?>
                    </select>
                  </div>
                  <!-- /select menu -->
                  <div class="form-group col-md-6">
                    <label for="menu">Submenu</label>
                    <input type="text" class="form-control" id="menu" name="menu" placeholder="Nama Submenu Baru">
                    <small><?= form_error('menu'); ?></small>
                  </div>
                </div>
                <div class="form-group">
                  <label for="url">Url</label>
                  <input type="text" class="form-control" id="url" name="url" placeholder="Url">
                  <small><?= form_error('url'); ?></small>
                </div>
                <div class="form-group">
                  <label for="iconsubmenu">Icon Submenu</label>
                  <input type="text" class="form-control" id="iconsubmenu" name="iconsubmenu" placeholder="iconsubmenu">
                  <small><?= form_error('iconsubmenu'); ?></small>
                </div>
                <!-- /.card-body -->
                <div class="row">
                  <div class="col-md-6">
                    <label for="is_active">is_active</label>
                    <input type="checkbox" name="is_active" id="is_active" value="1" checked>
                  </div>
                  <div class="card-footer col-md-6">
                    <button type="submit" class="btn btn-primary" name="submenuTambah">Submit</button>
                  </div>
                </div>
            </form>
          </div>
          <!-- /.card -->
        </div>
      </div>
    </div>

  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->