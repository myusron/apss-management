<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1><?= $title; ?></h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active"><?= $title; ?></li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-6">
          <div class="card card-primary">
            <div class="card-header">
            </div>
            <!-- /.card-header -->
            <!-- form start -->
            <form action="" method="post">
              <div class="card-body">
                <!-- <?php $i = $idbaru['id'] + 1; ?> -->
                <div class="form-group">
                  <label for="role">Code Categories</label>
                  <input type="hidden" name="id" id="id" value="<?= $detailcategories['id']; ?>">
                  <input type="text" class="form-control" id="codecategories" name="codecategories" placeholder="" readonly value="<?= $detailcategories['codecategories']; ?>">
                  <small><?= form_error('codecategories'); ?></small>
                </div>
                <div class="form-group">
                  <label for="role">Categories</label>
                  <input type="text" class="form-control" id="categories" name="categories" placeholder="" value="<?= $detailcategories['categories']; ?>">
                  <small><?= form_error('categories'); ?></small>
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary" name="">Simpan</button>
                  <a href="<?= base_url('products/categories'); ?>" class="btn btn-danger" name="">Cancel</a>
                </div>
            </form>
          </div>
          <!-- /.card -->
        </div>
      </div>
    </div>

  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->