<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1><?= $title; ?></h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active"><?= $title; ?></li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <!-- Main content -->
  <section class="content">

    <div class="card">
      <?= $this->session->flashdata('message'); ?>
      <div class="card-header">
        <h3 class="card-title">
          <a href="<?= base_url('products/tambahitems'); ?>" class="btn btn-primary">Tambah Items</a>
        </h3>
      </div>
      <!-- /.card-header -->
      <div class="card-body">
        <table class="table table-bordered" id="example1">
          <thead>
            <tr>
              <th style="width: 10px">#</th>
              <th>Kode Items</th>
              <th>Nama Items</th>
              <th>Deskripsi</th>
              <th>Harga</th>
              <th>Supliers</th>
              <th>Categories</th>
              <th>Units</th>
              <th>Stok</th>
              <th style="width: 120px">Action </th>
            </tr>
          </thead>
          <tbody>
            <?php $i = 1; ?>
            <?php foreach ($dataitems as $itm) : ?>
              <tr>
                <td><?= $i++; ?></td>
                <td><?= $itm['kode_items']; ?></td>
                <td><?= $itm['nama_items']; ?></td>
                <td><?= $itm['deskripsi']; ?></td>
                <td><?= $itm['harga']; ?></td>
                <td><?= $itm['nama_supliers']; ?></td>
                <td><?= $itm['categories']; ?></td>
                <td><?= $itm['units']; ?></td>
                <td><?= $itm['stok']; ?></td>
                <td>
                  <span class="badge bg-success"><a href="<?= base_url('products/edititems/') . $itm['id']; ?>">edit</a></span>
                  <span class="badge bg-danger"><a href="<?= base_url('products/hapusItems/') . $itm['id']; ?>" onclick="return confirm('apakah akan dihapus?')">delete</a></span>
                </td>
              </tr>
            <?php endforeach; ?>
          </tbody>
        </table>
      </div>
      <!-- /.card-body -->
    </div>
    <!-- /.card -->

  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->