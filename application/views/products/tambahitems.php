<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1><?= $title; ?></h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active"><?= $title; ?></li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-6">
          <div class="card card-primary">
            <div class="card-header">
            </div>
            <!-- /.card-header -->
            <!-- form start -->
            <form action="" method="post">
              <div class="card-body">
                <?php $kode = $iditems['id'] + 1; ?>
                <div class="row">
                  <div class="form-group col-lg-4">
                    <label for="kode_items">Kode Items</label>
                    <input type="text" class="form-control" id="kode_items" name="kode_items" placeholder="" readonly value="CODE00<?= $kode; ?>">
                    <small><?= form_error('kode_items'); ?></small>
                  </div>
                  <div class="form-group col-lg-8">
                    <label for="nama_items">Nama Items</label>
                    <input type="text" class="form-control" id="nama_items" name="nama_items" placeholder="" autofocus>
                    <small><?= form_error('nama_items'); ?></small>
                  </div>
                </div>
                <div class="row">
                  <div class="form-group col-lg-8">
                    <label for="deskripsi">Deskripsi</label>
                    <textarea type="text" class="form-control" id="deskripsi" name="deskripsi" placeholder=""></textarea>
                    <small><?= form_error('deskripsi'); ?></small>
                  </div>
                  <div class="form-group col-lg-4">
                    <label for="harga">Harga</label>
                    <input type="number" class="form-control" id="harga" name="harga" placeholder="">
                    <small><?= form_error('harga'); ?></small>
                  </div>
                </div>
                <div class="row">
                  <div class="form-group col-lg-4">
                    <label for="supliers_id">Supliers</label>
                    <select name="supliers_id" id="supliers_id" class="form-control">
                      <option value="">Pilih Supliers</option>
                      <?php foreach ($supliers as $splr) : ?>
                        <option value="<?= $splr['id']; ?>"><?= $splr['nama_supliers']; ?></option>
                      <?php endforeach; ?>
                    </select>
                    <small><?= form_error('supliers_id'); ?></small>
                  </div>
                  <div class="form-group col-lg-4">
                    <label for="categories_id">Categories</label>
                    <select name="categories_id" id="categories_id" class="form-control">
                      <option value="">Pilih Categories</option>
                      <?php foreach ($categories as $ctgr) : ?>
                        <option value="<?= $ctgr['id']; ?>"><?= $ctgr['categories']; ?></option>
                      <?php endforeach; ?>
                    </select>
                    <small><?= form_error('categories_id'); ?></small>
                  </div>
                  <div class="form-group col-lg-4">
                    <label for="units_id">Units</label>
                    <select name="units_id" id="units_id" class="form-control">
                      <option value="">Pilih Units</option>
                      <?php foreach ($units as $un) : ?>
                        <option value="<?= $un['id']; ?>"><?= $un['units']; ?></option>
                      <?php endforeach; ?>
                    </select>
                    <small><?= form_error('units_id'); ?></small>
                  </div>
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary" name="">Simpan</button>
                  <a href="<?= base_url('products/items'); ?>" class="btn btn-danger" name="">Cancel</a>
                </div>
            </form>
          </div>
          <!-- /.card -->
        </div>
      </div>
    </div>

  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->