<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1><?= $title; ?></h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active"><?= $title; ?></li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-lg-6">
        <div class="card" style="width: 18rem;">
          <div class="card-header">
            kepentingan Kriteria
            <ul class="list-group list-group-flush">
              <div class="row">
                <?php foreach ($kepentingankriteriaid1 as $kriteriaid1) : ?>
                  <li class="list-group-item">C1 : <?= $kriteriaid1->kepentingan_kriteria1; ?></li>
                <?php endforeach; ?>
                <?php foreach ($kepentingankriteriaid2 as $kriteriaid2) : ?>
                  <li class="list-group-item">C2 : <?= $kriteriaid2->kepentingan_kriteria2; ?></li>
                <?php endforeach; ?>
                <?php foreach ($kepentingankriteriaid3 as $kriteriaid3) : ?>
                  <li class="list-group-item">C3 : <?= $kriteriaid3->kepentingan_kriteria3; ?></li>
                <?php endforeach; ?>
                <?php foreach ($kepentingankriteriaid4 as $kriteriaid4) : ?>
                  <li class="list-group-item">C4 : <?= $kriteriaid4->kepentingan_kriteria4; ?></li>
                <?php endforeach; ?>
                <?php foreach ($kepentingankriteriaid5 as $kriteriaid5) : ?>
                  <li class="list-group-item">C5 : <?= $kriteriaid5->kepentingan_kriteria5; ?></li>
                <?php endforeach; ?>
                <?php foreach ($kepentingankriteriaid6 as $kriteriaid6) : ?>
                  <li class="list-group-item">C6 : <?= $kriteriaid6->kepentingan_kriteria6; ?></li>
                <?php endforeach; ?>
                <?php foreach ($kepentingankriteriaid7 as $kriteriaid7) : ?>
                  <li class="list-group-item">C7 : <?= $kriteriaid7->kepentingan_kriteria7; ?></li>
                <?php endforeach; ?>
                <?php foreach ($kepentingankriteriaid8 as $kriteriaid8) : ?>
                  <li class="list-group-item">C8 : <?= $kriteriaid8->kepentingan_kriteria8; ?></li>
                <?php endforeach; ?>
              </div>
            </ul>
          </div>
        </div>
      </div>
      <div class="col-lg-4">
        <div class="card" style="width: 18rem;">
          <div class="card-header">
            Jumlah Pembobotan
          </div>
          <ul class="list-group list-group-flush">
            <?php foreach ($countbobot as $row) : ?>
              <li class="list-group-item"><?= $row->sumkriteria; ?></li>
            <?php endforeach; ?>
          </ul>
        </div>
      </div>
    </div>
    <div class="card">
      <?php
      $c1 = $kriteriaid1->kepentingan_kriteria1 / $row->sumkriteria;
      $c2 = $kriteriaid2->kepentingan_kriteria2 / $row->sumkriteria;
      $c3 = $kriteriaid3->kepentingan_kriteria3 / $row->sumkriteria;
      $c4 = $kriteriaid4->kepentingan_kriteria4 / $row->sumkriteria;
      $c5 = $kriteriaid5->kepentingan_kriteria5 / $row->sumkriteria;
      $c6 = $kriteriaid6->kepentingan_kriteria6 / $row->sumkriteria;
      $c7 = $kriteriaid7->kepentingan_kriteria7 / $row->sumkriteria;
      $c8 = $kriteriaid8->kepentingan_kriteria8 / $row->sumkriteria;
      ?>
      <?= $this->session->flashdata('message'); ?>
      <div class="card-header">
        <h3 class="card-title">
          Bobot Kriteria
        </h3>
      </div>
      <!-- /.card-header -->
      <div class="card-body">
        <table class="table table-bordered" id="example1">
          <thead>
            <tr>
              <th style="width: 10px">Kriteria</th>
              <th>C1</th>
              <th>C2</th>
              <th>C3</th>
              <th>C4</th>
              <th>C5</th>
              <th>C6</th>
              <th>C7</th>
              <th>C8</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>Bobot Kepentingan</td>
              <td><?= round($c1, 3); ?></td>
              <td><?= round($c2, 3); ?></td>
              <td><?= round($c3, 3); ?></td>
              <td><?= round($c4, 3); ?></td>
              <td><?= round($c5, 3); ?></td>
              <td><?= round($c6, 3); ?></td>
              <td><?= round($c7, 3); ?></td>
              <td><?= round($c8, 3); ?></td>
            </tr>

          </tbody>
        </table>
      </div>
      <!-- /.card-body -->
    </div>
    <!-- /.card -->

  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->