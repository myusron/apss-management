<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1><?= $title; ?></h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active"><?= $title; ?></li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="info-box mb-3 bg-info">
      <span class="info-box-icon"><i class="far fa-comment"></i></span>

      <div class="info-box-content">
        <?php foreach ($countisinput as $row) : ?>
          <span class="info-box-text">Total Pelamar yang belum diinput data kriteria(C) : <b class="badge badge-danger"> <?= $row->is_input; ?> pelamar</b></span>
          <span class="info-box-number"><a class="btn btn-primary" href="<?= base_url('sistempenunjangkeputusan/lamarankerja'); ?>">Lihat data untuk melakukan input</a></span>
        <?php endforeach; ?>
        <span class="info-box-text"></span>
      </div>
      <!-- /.info-box-content -->
    </div>
    <div class="row">
      <div class="col-md-4">
        <div class="card">
          <div class="card-header">
            <h3 class="card-title">Kepentingan Kriteria</h3>
          </div>
          <!-- /.card-header -->
          <div class="card-body">
            <table class="table table-bordered" id="example2">
              <thead>
                <tr>
                  <th style="width: 10px">Bobot</th>
                  <th style="width: 30px">Kepentingan</th>
                </tr>
              </thead>
              <tbody>
                <?php $i = 1; ?>
                <?php foreach ($kepentingankriteria as $kkt) : ?>
                  <tr>
                    <td><?= $kkt['bobot']; ?></td>
                    <td><?= $kkt['kepentingan']; ?></td>
                  </tr>
                <?php endforeach; ?>
              </tbody>
            </table>
          </div>
          <!-- /.card-body -->
        </div>
      </div>
      <div class="col-md-3">
        <div class="card">
          <div class="card-header">
            <h3 class="card-title">Tipe Kriteria</h3>
          </div>
          <!-- /.card-header -->
          <div class="card-body">
            <table class="table table-bordered" id="example2">
              <thead>
                <tr>
                  <th style="width: 10px">#</th>
                  <th>Kriteria</th>
                  <th>Tipe</th>
                </tr>
              </thead>
              <tbody>
                <?php $i = 1; ?>
                <?php foreach ($tipekriteria as $tk) : ?>
                  <tr>
                    <td><?= $i++; ?></td>
                    <td><?= $tk['kriteria']; ?></td>
                    <td><?= $tk['tipe']; ?></td>
                  </tr>
                <?php endforeach; ?>
              </tbody>
            </table>
          </div>
          <!-- /.card-body -->
        </div>
      </div>
      <div class="col-md-5">
        <div class="card">
          <div class="card-header">
            <?php foreach ($countbobot as $row) : ?>
              <!-- total pembobotan -->
              <h5>Total Pembobotan : <?= $row->sumkriteria; ?></h5>
            <?php endforeach; ?>
          </div>
          <!-- /.card-header -->
          <div class="card-body">
            <table class="table table-bordered" id="example2">
              <thead>
                <tr>
                  <th style="width: 10px">#</th>
                  <th>Kriteria</th>
                  <th>Bobot</th>
                  <th>Keterangan</th>
                </tr>
              </thead>
              <tbody>
                <?php $i = 1; ?>
                <?php foreach ($joinpembobotankriteria as $jpk) : ?>
                  <tr>
                    <td><?= $i++; ?></td>
                    <td><?= $jpk['kriteria']; ?></td>
                    <td><?= $jpk['preferensi']; ?></td>
                    <td><?= $jpk['keterangan']; ?></td>
                  </tr>
                <?php endforeach; ?>
              </tbody>
            </table>
          </div>
          <!-- /.card-body -->
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-3">
        <div class="card" style="width: 18rem;">
          <div class="card-header">
            kepentingan Kriteria
            <ul class="list-group list-group-flush">
              <div class="row">
                <?php foreach ($kepentingankriteriaid1 as $kriteriaid1) : ?>
                  <li class="list-group-item">C1 : <?= $kriteriaid1->kepentingan_kriteria1; ?></li>
                <?php endforeach; ?>
                <?php foreach ($kepentingankriteriaid2 as $kriteriaid2) : ?>
                  <li class="list-group-item">C2 : <?= $kriteriaid2->kepentingan_kriteria2; ?></li>
                <?php endforeach; ?>
                <?php foreach ($kepentingankriteriaid3 as $kriteriaid3) : ?>
                  <li class="list-group-item">C3 : <?= $kriteriaid3->kepentingan_kriteria3; ?></li>
                <?php endforeach; ?>
                <?php foreach ($kepentingankriteriaid4 as $kriteriaid4) : ?>
                  <li class="list-group-item">C4 : <?= $kriteriaid4->kepentingan_kriteria4; ?></li>
                <?php endforeach; ?>
                <?php foreach ($kepentingankriteriaid5 as $kriteriaid5) : ?>
                  <li class="list-group-item">C5 : <?= $kriteriaid5->kepentingan_kriteria5; ?></li>
                <?php endforeach; ?>
                <?php foreach ($kepentingankriteriaid6 as $kriteriaid6) : ?>
                  <li class="list-group-item">C6 : <?= $kriteriaid6->kepentingan_kriteria6; ?></li>
                <?php endforeach; ?>
                <?php foreach ($kepentingankriteriaid7 as $kriteriaid7) : ?>
                  <li class="list-group-item">C7 : <?= $kriteriaid7->kepentingan_kriteria7; ?></li>
                <?php endforeach; ?>
                <?php foreach ($kepentingankriteriaid8 as $kriteriaid8) : ?>
                  <li class="list-group-item">C8 : <?= $kriteriaid8->kepentingan_kriteria8; ?></li>
                <?php endforeach; ?>
              </div>
            </ul>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <?php
          $c1 = $kriteriaid1->kepentingan_kriteria1 / $row->sumkriteria;
          $c2 = $kriteriaid2->kepentingan_kriteria2 / $row->sumkriteria;
          $c3 = $kriteriaid3->kepentingan_kriteria3 / $row->sumkriteria;
          $c4 = $kriteriaid4->kepentingan_kriteria4 / $row->sumkriteria;
          $c5 = $kriteriaid5->kepentingan_kriteria5 / $row->sumkriteria;
          $c6 = $kriteriaid6->kepentingan_kriteria6 / $row->sumkriteria;
          $c7 = $kriteriaid7->kepentingan_kriteria7 / $row->sumkriteria;
          $c8 = $kriteriaid8->kepentingan_kriteria8 / $row->sumkriteria;
          ?>
          <?= $this->session->flashdata('message'); ?>
          <div class="card-header">
            <h3 class="card-title">
              Bobot Kriteria
            </h3>
          </div>
          <!-- /.card-header -->
          <div class="card-body">
            <table class="table table-bordered" id="example2">
              <thead>
                <tr>
                  <th style="width: 10px">Kriteria</th>
                  <th>C1</th>
                  <th>C2</th>
                  <th>C3</th>
                  <th>C4</th>
                  <th>C5</th>
                  <th>C6</th>
                  <th>C7</th>
                  <th>C8</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>Bobot Kepentingan</td>
                  <td><?= round($c1, 3); ?></td>
                  <td><?= round($c2, 3); ?></td>
                  <td><?= round($c3, 3); ?></td>
                  <td><?= round($c4, 3); ?></td>
                  <td><?= round($c5, 3); ?></td>
                  <td><?= round($c6, 3); ?></td>
                  <td><?= round($c7, 3); ?></td>
                  <td><?= round($c8, 3); ?></td>
                </tr>

              </tbody>
            </table>
          </div>
          <!-- /.card-body -->
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-9">
        <div class="card">
          <div class="card-header">
            <h3 class="card-title">Alternatif</h3>
          </div>
          <!-- /.card-header -->
          <div class="card-body">
            <table class="table table-bordered" id="example2">
              <thead>
                <tr>
                  <th style="width: 10px">#</th>
                  <th>Nama Pelamar</th>
                  <th>Kriteria 1</th>
                  <th>Kriteria 2</th>
                  <th>Kriteria 3</th>
                  <th>Kriteria 4</th>
                  <th>Kriteria 5</th>
                  <th>Kriteria 6</th>
                  <th>Kriteria 7</th>
                  <th>Kriteria 8</th>
                </tr>
              </thead>
              <tbody>
                <?php $i = 1; ?>
                <?php foreach ($listlamarankerja as $llk) : ?>
                  <tr>
                    <td>A<?= $i++; ?></td>
                    <td><?= $llk['name']; ?></td>
                    <td><?= $llk['C1']; ?></td>
                    <td><?= $llk['C2']; ?></td>
                    <td><?= $llk['C3']; ?></td>
                    <td><?= $llk['C4']; ?></td>
                    <td><?= $llk['C5']; ?></td>
                    <td><?= $llk['C6']; ?></td>
                    <td><?= $llk['C7']; ?></td>
                    <td><?= $llk['C8']; ?></td>
                  </tr>
                <?php endforeach; ?>
              </tbody>
            </table>
          </div>
          <!-- /.card-body -->
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-4">
        <div class="card">
          <div class="card-header">
            <h3 class="card-title">Vektor S</h3>
          </div>
          <!-- /.card-header -->
          <div class="card-body">
            <table class="table table-bordered" id="example2">
              <thead>
                <tr>
                  <th style="width: 10px">#</th>
                  <th>Nama Pelamar</th>
                  <th>Vektor S</th>
                </tr>
              </thead>
              <tbody>
                <?php $i = 1; ?>
                <?php foreach ($listlamarankerja as $llk) : ?>
                  <tr>
                    <td>A<?= $i++; ?></td>
                    <td><?= $llk['name']; ?></td>
                    <td><?= pow($llk['C1'], $c1) * pow($llk['C2'], $c2) * pow($llk['C3'], $c3) * pow($llk['C4'], $c4) * pow($llk['C5'], $c5) * pow($llk['C6'], $c6) * pow($llk['C7'], $c7 * (-1)) * pow($llk['C8'], $c8 * (-1)); ?></td>
                  </tr>
                <?php endforeach; ?>
              </tbody>
            </table>
          </div>
          <!-- /.card-body -->
        </div>
      </div>
      <div class="col-md-4">
        <div class="card">
          <div class="card-header">
            <h3 class="card-title">Vektor V</h3>
          </div>
          <?php foreach ($C1id5 as $row); ?>
          <?php $k1c1 = pow($row->c1, $c1); ?>
          <?php foreach ($C2id5 as $row); ?>
          <?php $k2c2 = pow($row->c2, $c2); ?>
          <?php foreach ($C3id5 as $row); ?>
          <?php $k3c3 = pow($row->c3, $c3); ?>
          <?php foreach ($C4id5 as $row); ?>
          <?php $k4c4 = pow($row->c4, $c4); ?>
          <?php foreach ($C5id5 as $row); ?>
          <?php $k5c5 = pow($row->c5, $c5); ?>
          <?php foreach ($C6id5 as $row); ?>
          <?php $k6c6 = pow($row->c6, $c6); ?>
          <?php foreach ($C7id5 as $row); ?>
          <?php $k7c7 = pow($row->c7, $c7 * (-1)); ?>
          <?php foreach ($C8id5 as $row); ?>
          <?php $k8c8 = pow($row->c8, $c8 * (-1)); ?>
          <?php $vs1 = $k1c1 * $k2c2 * $k3c3 * $k4c4 * $k5c5 * $k6c6 * $k7c7 * $k8c8; ?>

          <?php foreach ($C1id6 as $row); ?>
          <?php $k1c1id6 = pow($row->c1, $c1); ?>
          <?php foreach ($C2id6 as $row); ?>
          <?php $k2c2id6 = pow($row->c2, $c2); ?>
          <?php foreach ($C3id6 as $row); ?>
          <?php $k3c3id6 = pow($row->c3, $c3); ?>
          <?php foreach ($C4id6 as $row); ?>
          <?php $k4c4id6 = pow($row->c4, $c4); ?>
          <?php foreach ($C5id6 as $row); ?>
          <?php $k5c5id6 = pow($row->c5, $c5); ?>
          <?php foreach ($C6id6 as $row); ?>
          <?php $k6c6id6 = pow($row->c6, $c6); ?>
          <?php foreach ($C7id6 as $row); ?>
          <?php $k7c7id6 = pow($row->c7, $c7 * (-1)); ?>
          <?php foreach ($C8id6 as $row); ?>
          <?php $k8c8id6 = pow($row->c8, $c8 * (-1)); ?>
          <?php $vs2 = $k1c1id6 * $k2c2id6 * $k3c3id6 * $k4c4id6 * $k5c5id6 * $k6c6id6 * $k7c7id6 * $k8c8id6; ?>

          <?php foreach ($C1id7 as $row); ?>
          <?php $k1c1id7 = pow($row->c1, $c1); ?>
          <?php foreach ($C2id7 as $row); ?>
          <?php $k2c2id7 = pow($row->c2, $c2); ?>
          <?php foreach ($C3id7 as $row); ?>
          <?php $k3c3id7 = pow($row->c3, $c3); ?>
          <?php foreach ($C4id7 as $row); ?>
          <?php $k4c4id7 = pow($row->c4, $c4); ?>
          <?php foreach ($C5id7 as $row); ?>
          <?php $k5c5id7 = pow($row->c5, $c5); ?>
          <?php foreach ($C6id7 as $row); ?>
          <?php $k6c6id7 = pow($row->c6, $c6); ?>
          <?php foreach ($C7id7 as $row); ?>
          <?php $k7c7id7 = pow($row->c7, $c7 * (-1)); ?>
          <?php foreach ($C8id7 as $row); ?>
          <?php $k8c8id7 = pow($row->c8, $c8 * (-1)); ?>
          <?php $vs3 = $k1c1id7 * $k2c2id7 * $k3c3id7 * $k4c4id7 * $k5c5id7 * $k6c6id7 * $k7c7id7 * $k8c8id7; ?>

          <?php foreach ($C1id8 as $row); ?>
          <?php $k1c1id8 = pow($row->c1, $c1); ?>
          <?php foreach ($C2id8 as $row); ?>
          <?php $k2c2id8 = pow($row->c2, $c2); ?>
          <?php foreach ($C3id8 as $row); ?>
          <?php $k3c3id8 = pow($row->c3, $c3); ?>
          <?php foreach ($C4id8 as $row); ?>
          <?php $k4c4id8 = pow($row->c4, $c4); ?>
          <?php foreach ($C5id8 as $row); ?>
          <?php $k5c5id8 = pow($row->c5, $c5); ?>
          <?php foreach ($C6id8 as $row); ?>
          <?php $k6c6id8 = pow($row->c6, $c6); ?>
          <?php foreach ($C7id8 as $row); ?>
          <?php $k7c7id8 = pow($row->c7, $c7 * (-1)); ?>
          <?php foreach ($C8id8 as $row); ?>
          <?php $k8c8id8 = pow($row->c8, $c8 * (-1)); ?>
          <?php $vs4 = $k1c1id8 * $k2c2id8 * $k3c3id8 * $k4c4id8 * $k5c5id8 * $k6c6id8 * $k7c7id8 * $k8c8id8; ?>

          <?php foreach ($C1id9 as $row); ?>
          <?php $k1c1id9 = pow($row->c1, $c1); ?>
          <?php foreach ($C2id9 as $row); ?>
          <?php $k2c2id9 = pow($row->c2, $c2); ?>
          <?php foreach ($C3id9 as $row); ?>
          <?php $k3c3id9 = pow($row->c3, $c3); ?>
          <?php foreach ($C4id9 as $row); ?>
          <?php $k4c4id9 = pow($row->c4, $c4); ?>
          <?php foreach ($C5id9 as $row); ?>
          <?php $k5c5id9 = pow($row->c5, $c5); ?>
          <?php foreach ($C6id9 as $row); ?>
          <?php $k6c6id9 = pow($row->c6, $c6); ?>
          <?php foreach ($C7id9 as $row); ?>
          <?php $k7c7id9 = pow($row->c7, $c7 * (-1)); ?>
          <?php foreach ($C8id9 as $row); ?>
          <?php $k8c8id9 = pow($row->c8, $c8 * (-1)); ?>
          <?php $vs5 = $k1c1id9 * $k2c2id9 * $k3c3id9 * $k4c4id9 * $k5c5id9 * $k6c6id9 * $k7c7id9 * $k8c8id9; ?>

          <?php foreach ($C1id10 as $row); ?>
          <?php $k1c1id10 = pow($row->c1, $c1); ?>
          <?php foreach ($C2id10 as $row); ?>
          <?php $k2c2id10 = pow($row->c2, $c2); ?>
          <?php foreach ($C3id10 as $row); ?>
          <?php $k3c3id10 = pow($row->c3, $c3); ?>
          <?php foreach ($C4id10 as $row); ?>
          <?php $k4c4id10 = pow($row->c4, $c4); ?>
          <?php foreach ($C5id10 as $row); ?>
          <?php $k5c5id10 = pow($row->c5, $c5); ?>
          <?php foreach ($C6id10 as $row); ?>
          <?php $k6c6id10 = pow($row->c6, $c6); ?>
          <?php foreach ($C7id10 as $row); ?>
          <?php $k7c7id10 = pow($row->c7, $c7 * (-1)); ?>
          <?php foreach ($C8id10 as $row); ?>
          <?php $k8c8id10 = pow($row->c8, $c8 * (-1)); ?>
          <?php $vs6 = $k1c1id10 * $k2c2id10 * $k3c3id10 * $k4c4id10 * $k5c5id10 * $k6c6id10 * $k7c7id10 * $k8c8id10; ?>

          <?php foreach ($C1id11 as $row); ?>
          <?php $k1c1id11 = pow($row->c1, $c1); ?>
          <?php foreach ($C2id11 as $row); ?>
          <?php $k2c2id11 = pow($row->c2, $c2); ?>
          <?php foreach ($C3id11 as $row); ?>
          <?php $k3c3id11 = pow($row->c3, $c3); ?>
          <?php foreach ($C4id11 as $row); ?>
          <?php $k4c4id11 = pow($row->c4, $c4); ?>
          <?php foreach ($C5id11 as $row); ?>
          <?php $k5c5id11 = pow($row->c5, $c5); ?>
          <?php foreach ($C6id11 as $row); ?>
          <?php $k6c6id11 = pow($row->c6, $c6); ?>
          <?php foreach ($C7id11 as $row); ?>
          <?php $k7c7id11 = pow($row->c7, $c7 * (-1)); ?>
          <?php foreach ($C8id11 as $row); ?>
          <?php $k8c8id11 = pow($row->c8, $c8 * (-1)); ?>
          <?php $vs7 = $k1c1id11 * $k2c2id11 * $k3c3id11 * $k4c4id11 * $k5c5id11 * $k6c6id11 * $k7c7id11 * $k8c8id11; ?>

          <?php $totalvektor = $vs1 + $vs2 + $vs3 + $vs4 + $vs5 + $vs6 + $vs7; ?>

          <?php $vektor1 = $vs1 / $totalvektor; ?>
          <?php $vektor2 = $vs2 / $totalvektor; ?>
          <?php $vektor3 = $vs3 / $totalvektor; ?>
          <?php $vektor4 = $vs4 / $totalvektor; ?>
          <?php $vektor5 = $vs5 / $totalvektor; ?>
          <?php $vektor6 = $vs6 / $totalvektor; ?>
          <?php $vektor7 = $vs7 / $totalvektor; ?>
          <!-- /.card-header -->
          <div class="card-body">
            <table class="table table-bordered" id="example2">
              <thead>
                <tr>
                  <th style="width: 10px">#</th>
                  <th>Nama Pelamar</th>
                  <th>Vektor V</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>A1</td>
                  <td>Muhammad yusron</td>
                  <td><?= $vektor1; ?></td>
                </tr>
                <tr>
                  <td>A2</td>
                  <td>Dewi</td>
                  <td><?= $vektor2; ?></td>
                </tr>
                <tr>
                  <td>A3</td>
                  <td>Adi Wiraga</td>
                  <td><?= $vektor3; ?></td>
                </tr>
                <tr>
                  <td>A4</td>
                  <td>Dedi Corbuzier</td>
                  <td><?= $vektor4; ?></td>
                </tr>
                <tr>
                  <td>A5</td>
                  <td>Raditiya Dika</td>
                  <td><?= $vektor5; ?></td>
                </tr>
                <tr>
                  <td>A6</td>
                  <td>Putri Tanjung</td>
                  <td><?= $vektor6; ?></td>
                </tr>
                <tr>
                  <td>A7</td>
                  <td>Tretan Muslim</td>
                  <td><?= $vektor7; ?></td>
                </tr>
              </tbody>
            </table>
          </div>
          <!-- /.card-body -->
        </div>
      </div>
      <div class="col-md-4">
        <div class="card">
          <div class="card-header">
            <h5>Short Vektor V Terbesar</h5>
          </div>
          <div class="card-body">
            <table class="table table-bordered" id="example2">
              <thead>
                <tr>
                  <th style="width: 10px">#</th>
                  <th>Nama Pelamar</th>
                  <th>Vektor V</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>A1</td>
                  <td>Muhammad yusron</td>
                  <td><?= $vektor1; ?></td>
                </tr>
                <tr>
                  <td>A2</td>
                  <td>Dewi</td>
                  <td><?= $vektor2; ?></td>
                </tr>
                <tr>
                  <td>A7</td>
                  <td>Tretan Muslim</td>
                  <td><?= $vektor7; ?></td>
                </tr>
                <tr>
                  <td>A3</td>
                  <td>Adi Wiraga</td>
                  <td><?= $vektor3; ?></td>
                </tr>
                <tr>
                  <td>A5</td>
                  <td>Raditiya Dika</td>
                  <td><?= $vektor5; ?></td>
                </tr>
                <tr>
                  <td>A4</td>
                  <td>Dedi Corbuzier</td>
                  <td><?= $vektor4; ?></td>
                </tr>
                <tr>
                  <td>A6</td>
                  <td>Putri Tanjung</td>
                  <td><?= $vektor6; ?></td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-4">
        <div class="card">
          <div class="card-header">
            <h3 class="card-title">Vektor V</h3>
          </div>
          <!-- /.card-header -->
          <div class="card-body">
            <table class="table table-bordered" id="example2">
              <thead>
                <tr>
                  <th style="width: 10px">#</th>
                  <th>Nama Pelamar</th>
                  <th>Vektor S</th>
                  <th>Vektor V</th>
                </tr>
              </thead>
              <tbody>
                <?php $i = 1; ?>
                <?php foreach ($listlamarankerja as $llk) : ?>
                  <tr>
                    <?php $vektors = pow($llk['C1'], $c1) * pow($llk['C2'], $c2) * pow($llk['C3'], $c3) * pow($llk['C4'], $c4) * pow($llk['C5'], $c5) * pow($llk['C6'], $c6) * pow($llk['C7'], $c7 * (-1)) * pow($llk['C8'], $c8 * (-1)); ?>
                    <td>A<?= $i++; ?></td>
                    <td><?= $llk['name']; ?></td>
                    <td><?= $vektors; ?></td>
                    <?php $totalvektors[] = $vektors ?>
                    <?php $hitungvektorv = array_sum($totalvektors) ?>
                    <?php 
                    $vektorv = $vektors / 170.221 //harusnya disini adalah total hitungan jumlah nilai vektor s
                    ?>
                    <td><?= $vektorv ?></td>
                  </tr>
                <?php endforeach; ?>
              </tbody>
            </table>
          </div>
          <!-- /.card-body -->
        </div>
      </div>
      <div class="col-md-4">
        <div class="card">
          <div class="card-header">
            <h3 class="card-title">Total Vektor S</h3>
          </div>
          <!-- /.card-header -->
          <div class="card-body">
            <table class="table table-bordered" id="example2">
              <thead>
                <tr>Total Vektor S : <?= $hitungvektorv; ?></tr>
                <br>
                <tr><?= $vektorv; ?></tr>
                <br>
                <tr><?= $vektors; ?></tr>
                </tr>
              </thead>
            </table>
          </div>
          <!-- /.card-body -->
        </div>
      </div>
    </div>
    <!-- /.card -->
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->