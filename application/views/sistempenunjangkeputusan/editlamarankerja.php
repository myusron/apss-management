<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1><?= $title; ?></h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active"><?= $title; ?></li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-6">
          <div class="card card-primary">
            <div class="card-header">
            </div>
            <!-- /.card-header -->
            <!-- form start -->
            <form action="" method="post">
              <input type="hidden" name="id" id="id" value="<?= $kriteria['id']; ?>">
              <input type="hidden" name="user_id" id="user_id" value="<?= $kriteria['user_id']; ?>">
              <input type="hidden" name="upload_berkas" id="upload_berkas" value="<?= $kriteria['upload_berkas']; ?>">
              <input type="hidden" name="is_input" id="is_input" value="<?= $kriteria['is_input']; ?>">
              <div class="card-body">
                <div class="row">
                  <div class="col-md-3">
                    <div class="form-group">
                      <label for="C1">Kriteria 1</label>
                      <input type="text" class="form-control" id="C1" name="C1" placeholder="" value="<?= $kriteria['C1']; ?>">
                      <small><?= form_error('C1'); ?></small>
                    </div>
                  </div>
                  <div class="col-md-3">
                    <div class="form-group">
                      <label for="C2">Kriteria 2</label>
                      <input type="text" class="form-control" id="C2" name="C2" placeholder="" value="<?= $kriteria['C2']; ?>">
                      <small><?= form_error('C2'); ?></small>
                    </div>
                  </div>
                  <div class="col-md-3">
                    <div class="form-group">
                      <label for="C3">Kriteria 3</label>
                      <input type="text" class="form-control" id="C3" name="C3" placeholder="" value="<?= $kriteria['C3']; ?>">
                      <small><?= form_error('C3'); ?></small>
                    </div>
                  </div>
                  <div class="col-md-3">
                    <div class="form-group">
                      <label for="C4">Kriteria 4</label>
                      <input type="text" class="form-control" id="C4" name="C4" placeholder="" value="<?= $kriteria['C4']; ?>">
                      <small><?= form_error('C4'); ?></small>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-3">
                    <div class="form-group">
                      <label for="C5">Kriteria 5</label>
                      <input type="text" class="form-control" id="C5" name="C5" placeholder="" value="<?= $kriteria['C5']; ?>">
                      <small><?= form_error('C5'); ?></small>
                    </div>
                  </div>
                  <div class="col-md-3">
                    <div class="form-group">
                      <label for="C6">Kriteria 6</label>
                      <input type="text" class="form-control" id="C6" name="C6" placeholder="" value="<?= $kriteria['C6']; ?>">
                      <small><?= form_error('C6'); ?></small>
                    </div>
                  </div>
                  <div class="col-md-3">
                    <div class="form-group">
                      <label for="C7">Kriteria 7</label>
                      <input type="text" class="form-control" id="C7" name="C7" placeholder="" value="<?= $kriteria['C7']; ?>">
                      <small><?= form_error('C7'); ?></small>
                    </div>
                  </div>
                  <div class="col-md-3">
                    <div class="form-group">
                      <label for="C8">Kriteria 8</label>
                      <input type="text" class="form-control" id="C8" name="C8" placeholder="" value="<?= $kriteria['C8']; ?>">
                      <small><?= form_error('C8'); ?></small>
                    </div>
                  </div>
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary" name="">Submit</button>
                </div>
            </form>
          </div>
          <!-- /.card -->
        </div>
      </div>
    </div>

  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->