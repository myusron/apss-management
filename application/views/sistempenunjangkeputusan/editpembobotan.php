<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1><?= $title; ?></h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active"><?= $title; ?></li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-5">
          <div class="card card-primary">
            <div class="card-header">
            </div>
            <!-- /.card-header -->
            <!-- form start -->
            <form action="" method="post">
              <div class="card-body">
                <div class="form-group">
                  <input type="hidden" name="id" id="id" value="<?= $pembobotan['id']; ?>">
                  <label for="tipe_kriteria_id">Tipe Kriteria</label>
                  <select name="tipe_kriteria_id" id="tipe_kriteria_id" class="form-control">
                    <?php foreach ($tipekriteria as $tk) : ?>
                      <?php if ($pembobotan['tipe_kriteria_id'] == $tk['id']) : ?>
                        <option value="<?= $tk['id']; ?>" selected><?= $tk['kriteria']; ?></option>
                      <?php else : ?>
                        <option value="<?= $tk['id']; ?>"><?= $tk['kriteria']; ?></option>
                      <?php endif; ?>
                    <?php endforeach; ?>
                  </select>
                  <small><?= form_error('tipe_kriteria_id'); ?></small>
                </div>
                <div class="form-group">
                  <label for="kepentingan_kriteria_id">Kepentingan Kriteria</label>
                  <select name="kepentingan_kriteria_id" id="kepentingan_kriteria_id" class="form-control">
                    <?php foreach ($kepentingankriteria as $kp) : ?>
                      <?php if ($pembobotan['kepentingan_kriteria_id'] == $kp['id']) : ?>
                        <option value="<?= $kp['id']; ?>" selected><?= $kp['bobot']; ?></option>
                      <?php else : ?>
                        <option value="<?= $kp['id']; ?>"><?= $kp['bobot']; ?></option>
                      <?php endif; ?>
                    <?php endforeach; ?>
                  </select>
                  <small><?= form_error('kepentingan_kriteria_id'); ?></small>
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary" name="">Submit</button>
                </div>
            </form>
          </div>
          <!-- /.card -->
        </div>
        <div class="col-md-4">
          <div class="card" style="width: 18rem;">
            <div class="card-header">
              Petunjuk Kepentingan Kriteria
            </div>
            <ul class="list-group list-group-flush">
              <?php foreach ($kepentingankriteria as $kkt) : ?>
                <li class="list-group-item"><?= $kkt['bobot']; ?> || <?= $kkt['kepentingan']; ?></li>
              <?php endforeach; ?>
            </ul>
          </div>
        </div>
      </div>
    </div>

  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->