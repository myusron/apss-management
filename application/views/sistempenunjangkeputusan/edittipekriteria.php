<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1><?= $title; ?></h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active"><?= $title; ?></li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-6">
          <div class="card card-primary">
            <div class="card-header">
            </div>
            <!-- /.card-header -->
            <!-- form start -->
            <form action="" method="post">
              <div class="card-body">
                <div class="form-group">
                  <input type="hidden" name="id" id="id" value="<?= $tipekriteria['id']; ?>">
                  <label for="kriteria">Kriteria</label>
                  <input type="text" class="form-control" id="kriteria" name="kriteria" placeholder="" value="<?= $tipekriteria['kriteria']; ?>">
                  <small><?= form_error('kriteria'); ?></small>
                </div>
                <div class="form-group">
                  <label for="tipe">Tipe</label>
                  <input type="text" class="form-control" id="tipe" name="tipe" placeholder="" value="<?= $tipekriteria['tipe']; ?>">
                  <small><?= form_error('tipe'); ?></small>
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary" name="">Submit</button>
                </div>
            </form>
          </div>
          <!-- /.card -->
        </div>
      </div>
    </div>

  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->