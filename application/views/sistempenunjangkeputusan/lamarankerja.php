<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1><?= $title; ?></h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active"><?= $title; ?></li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <!-- Main content -->
  <section class="content">

    <div class="card">
      <?= $this->session->flashdata('message'); ?>
      <div class="card-header">
        <h3 class="card-title">
          <a href="<?= base_url('admin/tambahlamarankerja'); ?>" class="btn btn-primary">Tambah Lamaran Kerja</a>
        </h3>
      </div>
      <!-- /.card-header -->
      <div class="card-body">
        <table class="table table-bordered" id="example1">
          <thead>
            <tr>
              <th style="width: 10px">#</th>
              <th>Nama Pelamar</th>
              <th style="width: 190px">Berkas</th>
              <th>Status Input</th>
              <th style="width: 120px">Action </th>
              <th>Kriteria</th>
            </tr>
          </thead>
          <tbody>
            <?php $i = 1; ?>
            <?php foreach ($listlamarankerja as $llk) : ?>
              <tr>
                <td><?= $i++; ?></td>
                <td><?= $llk['name']; ?></td>
                <td><a href="<?= base_url('uploads/') . $llk['upload_berkas']; ?>" download=""><?= $llk['upload_berkas']; ?></a></td>
                <td><?= $llk['is_input']; ?></td>
                <td>
                  <span class="badge bg-success"><a href="<?= base_url('sistempenunjangkeputusan/editlamarankerja/') . $llk['id']; ?>">edit</a></span>
                  <span class="badge bg-danger"><a href="<?= base_url('sistempenunjangkeputusan/hapusLamarankerja/') . $llk['id']; ?>" onclick="return confirm('apakah akan dihapus?')">delete</a></span>
                  <?php if ($llk['is_input'] == 0) : ?>
                    <span class="badge bg-warning"><a href="<?= base_url('sistempenunjangkeputusan/editlamarankerja/') . $llk['id']; ?>">input kriteria</a></span>
                  <?php endif; ?>
                </td>
                <td>C1 : <?= $llk['C1']; ?> |
                  C2 : <?= $llk['C2']; ?> |
                  C3 : <?= $llk['C3']; ?> |
                  C4 : <?= $llk['C4']; ?> |
                  C5 : <?= $llk['C5']; ?> |
                  C6 : <?= $llk['C6']; ?> |
                  C7 : <?= $llk['C7']; ?> |
                  C8 : <?= $llk['C8']; ?></td>
              </tr>
            <?php endforeach; ?>
          </tbody>
        </table>
      </div>
      <!-- /.card-body -->
    </div>
    <!-- /.card -->

  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->