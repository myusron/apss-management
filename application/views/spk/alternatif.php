<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1><?= $title; ?></h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active"><?= $title; ?></li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <!-- Main content -->
  <section class="content">

    <div class="card">
      <?= $this->session->flashdata('message'); ?>
      <div class="card-header">
        <h3 class="card-title">
          <a href="<?= base_url('spk/tambahalternatif'); ?>" class="btn btn-primary">Tambah Alternatif</a>
        </h3>
      </div>
      <!-- /.card-header -->
      <div class="card-body">
        <table class="table table-bordered" id="example1">
          <thead>
            <tr>
              <th style="width: 10px">#</th>
              <th>Alternatif</th>
              <th>Keterangan</th>
              <th style="width: 120px">Action </th>
            </tr>
          </thead>
          <tbody>
            <?php $i = 1; ?>
            <?php foreach ($alternatif as $al) : ?>
              <tr>
                <td><?= $i++; ?></td>
                <td><?= $al['alternatif']; ?></td>
                <td><?= $al['keterangan']; ?></td>
                <td>
                  <span class="badge bg-success"><a href="<?= base_url('spk/editalternatif/') . $al['id']; ?>">edit</a></span>
                  <span class="badge bg-danger"><a href="<?= base_url('spk/deleteAlternatif/') . $al['id']; ?>" onclick="return confirm('apakah akan dihapus?')">delete</a></span>
                  <?php if ($al['is_input'] == 0) : ?>
                    <span class="badge bg-warning"><a href="<?= base_url('spk/inputkriteria/'); ?>">input</a></span>
                  <?php endif; ?>
                </td>
              </tr>
            <?php endforeach; ?>
          </tbody>
        </table>
      </div>
      <!-- /.card-body -->
    </div>
    <!-- /.card -->

  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->