<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1><?= $title; ?></h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active"><?= $title; ?></li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-6">
          <div class="card card-primary">
            <div class="card-header">
            </div>
            <!-- /.card-header -->
            <!-- form start -->
            <form action="" method="post">
              <div class="card-body">
                <div class="form-group">
                  <input type="hidden" name="id" id="id" value="<?= $bobotkriteria['id']; ?>">
                  <select name="spk_kriteria_id" id="spk_kriteria_id">
                    <?php foreach ($kriteria as $k) : ?>
                      <?php if ($k['id'] == $bobotkriteria['spk_kriteria_id']) : ?>
                        <option value="<?= $k['id']; ?>" selected><?= $k['kriteria']; ?></option>
                      <?php else : ?>
                        <option value="<?= $k['id']; ?>"><?= $k['kriteria']; ?></option>
                      <?php endif; ?>
                    <?php endforeach; ?>
                  </select>
                  <label for="spk_kriteria_id">Kriteria</label>
                  <small><?= form_error('spk_kriteria_id'); ?></small>
                </div>
                <div class="form-group">
                  <label for="spk_bobot_id">Bobot</label>
                  <select name="spk_bobot_id" id="spk_bobot_id">
                    <?php foreach ($bobot as $b) : ?>
                      <?php if ($b['id'] == $bobotkriteria['spk_bobot_id']) : ?>
                        <option value="<?= $b['id']; ?>" selected><?= $b['bobot']; ?></option>
                      <?php else : ?>
                        <option value="<?= $b['id']; ?>"><?= $b['bobot']; ?></option>
                      <?php endif; ?>
                    <?php endforeach; ?>
                  </select>
                  <small><?= form_error('spk_bobot_id'); ?></small>
                </div>
                <div class="form-group">
                  <label for="tipe">Tipe</label>
                  <input type="text" class="form-control" id="tipe" name="tipe" placeholder="" value="<?= $kriteria['tipe']; ?>">
                  <small><?= form_error('tipe'); ?></small>
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary" name="">Submit</button>
                </div>
            </form>
          </div>
          <!-- /.card -->
        </div>
      </div>
    </div>

  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->