<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1><?= $title; ?></h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active"><?= $title; ?></li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-6">
          <div class="card card-primary">
            <div class="card-header">
            </div>
            <!-- /.card-header -->
            <!-- form start -->
            <form action="" method="post">
              <div class="card-body" id="card-body">
                <br>
                <div class="form-group row" id="form-group_1">
                  <div class="col-sm-4">
                    <select name="alternatif_id" id="alternatif_id" class="form-control">
                      <?php foreach ($alternatif as $alt) : ?>
                        <option value="<?= $alt['id']; ?>"><?= $alt['alternatif']; ?> | <?= $alt['keterangan']; ?></option>
                      <?php endforeach; ?>
                    </select>
                  </div>
                  <div class="col-sm-4">
                    <select name="kriteria_id" id="kriteria_id" class="form-control">
                      <?php foreach ($kriteria as $k) : ?>
                        <option value="<?= $k['id']; ?>"><?= $k['kriteria']; ?> | <?= $k['kode']; ?></option>
                      <?php endforeach; ?>
                    </select>
                  </div>
                  <div class="col-sm-4">
                    <input type="text" class="form-control" id="nilai_kriteria" name="nilai_kriteria" placeholder="" autofocus>
                  </div>
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary" name="">Simpan</button>
                </div>
              </div>
            </form>
          </div>
          <!-- /.card -->
        </div>
      </div>
    </div>

  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->