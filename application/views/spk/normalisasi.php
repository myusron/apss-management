<?php
$query = "select
sum(b.bobot) as bobot
from
spk_bobot_kriteria a
join spk_bobot b on a.spk_bobot_id=b.id
join spk_kriteria c on a.spk_kriteria_id=c.id";
$totalbobot = $this->db->query($query)->result();
?>
<?php foreach ($totalbobot as $row) ?>
<?php
// $totalbobot = $this->db->query("select sum(`bobot`) as `bobot` from `spk_bobot`")->result_array();
$query = "select 
`a`.`id`,
`b`.`kriteria` as kriteria,
`b`.`kode` as kode,
`c`.`bobot` as bobot,
`c`.`bobot`/ $row->bobot as normalisasi
from `spk_bobot_kriteria` `a`
join `spk_kriteria` `b` on `a`.`spk_kriteria_id` = `b`.`id`
join `spk_bobot` c on `a`.`spk_bobot_id` = `c`.`id`
order by `b`.`id`";
$normalisasi = $this->db->query($query)->result();
?>

<?php
$query = "select 
a.id,
b.kriteria as kriteria,
b.kode as kode,
c.bobot as bobot,
c.kepentingan as kepentingan
from spk_bobot_kriteria a
join spk_kriteria b on a.spk_kriteria_id = b.id
join spk_bobot c on a.spk_bobot_id = c.id
order by b.id";
$joinbobotkriteria = $this->db->query($query)->result_array();
?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1><?= $title; ?></h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active"><?= $title; ?></li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-lg-4">
        <div class="card">
          <?= $this->session->flashdata('message'); ?>
          <div class="card-header">
            <h3 class="card-title">
              <!-- button -->
              Hasil Normalisasi
            </h3>
          </div>
          <!-- /.card-header -->
          <div class="card-body">
            <table class="table table-bordered" id="">
              <thead>
                <tr>
                  <th style="width: 10px">#</th>
                  <th>Kriteria</th>
                  <th>bobot</th>
                  <th>Normalisasi</th>
                </tr>
              </thead>
              <tbody>
                <?php
                $i = 1;
                ?>
                <?php foreach ($normalisasi as $row) : ?>
                  <tr>
                    <td><?= $i++; ?></td>
                    <td><?= $row->kriteria; ?> | <?= $row->kode; ?></td>
                    <td><?= $row->bobot; ?></td>
                    <td><?= round($row->normalisasi, 3); ?></td>
                  </tr>
                <?php endforeach; ?>
              </tbody>
            </table>
          </div>
          <!-- /.card-body -->
        </div>
      </div>
      <div class="col-lg-2">
        <div class="card">
          <?= $this->session->flashdata('message'); ?>
          <div class="card-header">
            <h3 class="card-title">
              <!-- button -->
              Total pembobotan
            </h3>
          </div>
          <!-- /.card-header -->
          <div class="card-body">
            <table class="table table-bordered" id="">
              <thead>
                <tr>
                  <?php foreach ($totalbobot as $row); ?>
                  <th><?= $row->bobot; ?></th>
                </tr>
              </thead>
            </table>
          </div>
          <!-- /.card-body -->
        </div>
      </div>
    </div>
    <!-- /.card -->
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->