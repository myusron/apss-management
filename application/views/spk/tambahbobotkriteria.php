<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1><?= $title; ?></h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active"><?= $title; ?></li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-6">
          <div class="card card-primary">
            <div class="card-header">
            </div>
            <!-- /.card-header -->
            <!-- form start -->
            <form action="" method="post">
              <div class="card-body">
                <div class="form-group">
                  <label for="spk_kriteria_id">Kriteria</label>
                  <select name="spk_kriteria_id" id="spk_kriteria_id" class="form-control">
                    <option value="">Pilih Kriteria</option>
                    <?php foreach ($kriteria as $k) : ?>
                      <option value="<?= $k['id']; ?>"><?= $k['kriteria']; ?></option>
                    <?php endforeach; ?>
                  </select>
                  <small><?= form_error('spk_kriteria_id'); ?></small>
                </div>
                <div class="form-group">
                  <label for="spk_bobot_id">Bobot</label>
                  <select name="spk_bobot_id" id="spk_bobot_id" class="form-control">
                    <option value="">Pilih Bobot</option>
                    <?php foreach ($bobot as $b) : ?>
                      <option value="<?= $b['id']; ?>"><?= $b['bobot']; ?></option>
                    <?php endforeach; ?>
                  </select>
                  <small><?= form_error('spk_bobot_id'); ?></small>
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary" name="">Submit</button>
                </div>
            </form>
          </div>
          <!-- /.card -->
        </div>
      </div>
    </div>

  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->