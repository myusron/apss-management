<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-dark-primary elevation-4">
  <!-- Brand Logo -->
  <a href="<?= base_url(); ?>" class="brand-link">
    <img src="<?= base_url('assets/'); ?>dist/img/AdminLTELogo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
    <span class="brand-text font-weight-light">Point of Sales</span>
  </a>

  <!-- Sidebar -->
  <div class="sidebar">

    <!-- Sidebar Menu -->

    <?php
    $role_id = $this->session->userdata('role_id');
    $queryMenu = "select `user_menu`.`id`,`user_menu`.`icon`, `menu`
                    from `user_menu` join `user_access_menu`
                    on `user_menu`.`id` = `user_access_menu`.`menu_id`
                    where `user_access_menu`.`role_id` = $role_id
                    order by `user_access_menu`.`menu_id` asc
                    ";
    $menu = $this->db->query($queryMenu)->result_array();
    ?>
    <?php foreach ($menu as $m) : ?>
      <?php
      $menuId = $m['id'];
      $querySubmenu = "select * from `user_sub_menu`
                              join `user_menu`
                              on `user_sub_menu`.`menu_id` = `user_menu`.`id`
                              where `user_sub_menu`.`menu_id` = $menuId
                              and `user_sub_menu`.`is_active` = 1
                            ";
      $subMenu = $this->db->query($querySubmenu)->result_array();
      ?>
      <nav>
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <li class="nav-item">
            <a href="" class="nav-link">
              <?= $m['menu']; ?>
              <!-- <i class="fas fa-angle-left right"></i> -->
            </a>
            <!-- //nav-treeview -->
            <ul class="nav">
              <li class="nav-item">
                <?php foreach ($subMenu as $sm) : ?>
                  <a href="<?= base_url($sm['url']); ?>" class="nav-link">
                    <i class="<?= $sm['iconsubmenu']; ?> nav-icon"></i>
                    <p><?= $sm['title']; ?></p>
                  </a>
                <?php endforeach; ?>
              </li>
            </ul>
          </li>
        </ul>
        <hr>
      </nav>
    <?php endforeach; ?>
    <!-- /.sidebar-menu -->
    <!-- /.sidebar -->

    <div class="card-body box-profile">
      <?php
      $id = $this->session->userdata('role_id');
      $query = "select `user`.`name`, `user`.`role_id`, `user_role`.`role`
  from `user`
  join `user_role`
  on `user`.`role_id`=`user_role`.`id`
  where `user`.`id` = $id
  ";
      $result = $this->db->query($query)->result_array();

      ?>
      <div class="text-center">
        <p>Your identity</p>
        <img class="profile-user-img img-fluid img-circle" src="<?= base_url('assets/dist/img/profile/') . $user['image']; ?>" alt="User profile picture">
      </div>

      <h3 class="profile-username text-center"><?= $user['name']; ?></h3>

      <p class="text-muted text-center"><?= $user['email']; ?></p>

      <ul class="list-group list-group-unbordered mb-3 text-center">
        <li class="list-group-item text-center">
          <?php foreach ($result as $r) : ?>
            <b>Role : </b> <a class="">
              <!-- <?= $user['role_id']; ?> -->
              <?= $r['name']; ?>
            </a>
          <?php endforeach; ?>
        </li>
        <li class="list-group-item text-center">
          <b>Active since : </b> <a class=""><?= date('d F Y', $user['date_created']); ?></a>
        </li>
        <br>
        <i class="fas fa-medal"></i>
      </ul>
    </div>
</aside>