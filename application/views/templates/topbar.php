<div class="wrapper">

  <!-- Preloader -->
  <!-- <div class="preloader flex-column justify-content-center align-items-center">
    <h1>
      Point Of Sales
    </h1>
  </div> -->

  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-dark">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="index3.html" class="nav-link">Hai, <?= $user['name']; ?></a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <img src="<?= base_url('assets/dist/img/profile/') . $user['image']; ?>" class="img-thumbnail" style="width: 50px; height: 50px;" alt="User Image">
      </li>
    </ul>

    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
      <!-- Navbar Search -->

      <li class="nav-item">
        <!-- <a class="nav-link" data-widget="control-sidebar" data-slide="true" href="#" role="button">
          <i class="fas fa-th-large"></i>
        </a> -->
        <a href="<?= base_url('auth/logout'); ?>" onclick="confirm('Yakin akan logout?');" class="">
          logout <i class="fas fa-arrow-right text-muted"></i>
        </a>
      </li>
    </ul>
  </nav>
  <!-- /.navbar -->