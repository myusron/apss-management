<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1><?= $title; ?></h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active"><?= $title; ?></li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-6">
          <div class="card card-primary">
            <div class="card-header">
            </div>
            <!-- /.card-header -->
            <!-- form start -->
            <form action="" method="post">
              <div class="card-body">
                <div class="form-group">
                  <label for="items_id">Nama Items</label>
                  <select name="items_id" id="items_id" class="form-control">
                    <option value="">Pilih Items</option>
                    <?php foreach ($items as $it) : ?>
                      <option value="<?= $it['id'] ?>"><?= $it['kode_items'] ?> | <?= $it['nama_items'] ?></option>
                    <?php endforeach; ?>
                  </select>
                  <small><?= form_error('items_id'); ?></small>
                </div>
                <div class="form-group">
                  <label for="stok_in">Stok In</label>
                  <input type="number" class="form-control" id="stok_in" name="stok_in" placeholder="">
                  <small><?= form_error('stok_in'); ?></small>
                </div>
                <div class="form-group">
                  <label for="supliers_id">Alamat Supliers</label>
                  <select name="supliers_id" id="supliers_id" class="form-control">
                    <option value="">Pilih Supliers</option>
                    <?php foreach ($supliers as $spl) : ?>
                      <option value="<?= $spl['id'] ?>"><?= $spl['kode_supliers'] ?><?= $spl['nama_supliers'] ?></option>
                    <?php endforeach; ?>
                  </select>
                  <small><?= form_error('supliers_id'); ?></small>
                </div>
                <div class="form-group">
                  <label for="keterangan">Keterangan</label>
                  <Textarea id="keterangan" name="keterangan" class="form-control"></Textarea>
                  <small><?= form_error('keterangan'); ?></small>
                </div>
                <input type="hidden" name="is_active" id="is_active">
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary" name="">Simpan</button>
                  <a href="<?= base_url('master/supliers'); ?>" class="btn btn-danger" name="">Cancel</a>
                </div>
            </form>
          </div>
          <!-- /.card -->
        </div>
      </div>
    </div>

  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->