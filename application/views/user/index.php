<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <?php
  $id = $this->session->userdata('role_id');
  $query = "select `user`.`name`, `user`.`role_id`, `user_role`.`role`
  from `user`
  join `user_role`
  on `user`.`role_id`=`user_role`.`id`
  where `user`.`role_id` = $id
  ";
  $result = $this->db->query($query)->result_array();

  ?>
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1><?= $title; ?></h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active"><?= $title; ?></li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-lg-3">
          <?= $this->session->flashdata('message'); ?>

          <!-- Profile Image -->
          <div class="card card-primary card-outline">
            <div class="card-body box-profile">
              <div class="text-center">
                <img class="profile-user-img img-fluid img-circle" src="<?= base_url('assets/dist/img/profile/') . $user['image']; ?>" alt="User profile picture">
              </div>

              <h3 class="profile-username text-center"><?= $user['name']; ?></h3>

              <p class="text-muted text-center"><?= $user['email']; ?></p>

              <ul class="list-group list-group-unbordered mb-3 text-center">
                <li class="list-group-item text-center">

                  <b>Role : </b> <a class="">
                    <?= $roleketerangan['role']; ?>
                  </a>

                </li>
                <li class="list-group-item text-center">
                  <b>Active since : </b> <a class=""><?= date('d F Y', $user['date_created']); ?></a>
                </li>
              </ul>

              <a href="<?= base_url('user/ubahdatauser/'); ?>" class="btn btn-primary btn-block">Ubah</a>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <div class="col-lg-9">
          <div class="row">
            <div class="col-md-5">
              <img src="<?= base_url('assets/dist/img/loker.jpg'); ?>" alt="..." style="width: 250px; height: 250px;">
            </div>
            <div class="col-md-4">
              <div class="card-body">
                <h5 class="card-title">Info Lowongan Kerja</h5>
                <hr>
                <p class="card-text">Lorem ipsum dolor sit amet consectetur, adipisicing elit. Sit accusantium ea inventore explicabo! In, adipisci enim rem debitis consequatur illo! This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
                <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
                <hr>
                <a href="<?= base_url('user/tambahlamarankerja'); ?>" class="btn btn-primary">Input Lamar Kerja</a>
              </div>
            </div>
          </div>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </div><!-- /.container-fluid -->
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->