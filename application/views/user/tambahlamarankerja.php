<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1><?= $title; ?></h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active"><?= $title; ?></li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-6">
          <div class="card card-primary">
            <div class="card-header">
              <?= $this->session->flashdata('message'); ?>
            </div>
            <!-- /.card-header -->
            <!-- form start -->
            <?php echo form_open_multipart('user/tambahlamarankerja'); ?>
            <div class="card-body">
              <div class="form-group">
                <label for="user_id">Hai, <b><?= $user['name']; ?> silahkan upload lamaran kerja anda yang berbentuk pdf berisi CV, Ijazah serta berkas lain.</b></label>
                <input type="hidden" class="form-control" id="user_id" name="user_id" placeholder="" readonly value="<?= $user['id']; ?>">
                <small><?= form_error('user_id'); ?></small>
              </div>
              <div class="form-group">
                <label for="upload_berkas">Upload Berkas</label>
                <input type="file" class="form-control" id="upload_berkas" name="upload_berkas">
                <small><?= form_error('upload_berkas'); ?></small>
              </div>
              <input type="hidden" name="is_input" id="is_input" value="0">
              <!-- /.card-body -->

              <div class="card-footer">
                <button type="submit" class="btn btn-primary" name="">Submit</button>
              </div>
              </form>
            </div>
            <!-- /.card -->
          </div>
        </div>
      </div>

  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->