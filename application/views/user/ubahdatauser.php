<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1><?= $title; ?></h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active"><?= $title; ?></li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-6">
          <div class="card card-primary">
            <div class="card-header">
            </div>
            <!-- /.card-header -->
            <!-- form start -->
            <!-- <form action="" method="post"> -->
            <?= form_open_multipart('user/ubahDataUser'); ?>
            <div class="card-body">
              <input type="hidden" name="id" id="id" value="<?= $user['id']; ?>">
              <input type="hidden" name="password" id="password" value="<?= $user['password']; ?>">
              <input type="hidden" name="role_id" id="role_id" value="<?= $user['role_id']; ?>">
              <input type="hidden" name="is_active" id="is_active" value="<?= $user['is_active']; ?>">
              <input type="hidden" name="date_created" id="date_created" value="<?= $user['date_created']; ?>">
              <div class="form-group">
                <input type="text" class="form-control" id="name" name="name" value="<?= $user['name']; ?>">
                <small><?= form_error('name'); ?></small>
              </div>
              <div class="form-group">
                <input type="text" class="form-control" id="email" name="email" value="<?= $user['email']; ?>">
                <small><?= form_error('email'); ?></small>
              </div>
              <div class="form-group row">
                <div class="col-sm-2">Picture</div>
                <div class="col-sm-10">
                  <div class="row">
                    <div class="col-sm-3">
                      <img src="<?= base_url('assets/dist/img/profile/') . $user['image']; ?>" class="img-thumbnail">
                    </div>
                    <div class="col-sm-9">
                      <div class="custom-file">
                        <input type="file" name="image" id="image">
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <!-- /.card-body -->

              <div class="card-footer">
                <button type="submit" class="btn btn-primary" name="">Submit</button>
              </div>
              </form>
            </div>
            <!-- /.card -->
          </div>
        </div>
      </div>

  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->