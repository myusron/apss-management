const base_url = "http://localhost/pointofsales/";
console.log(base_url);
$(document).ready(function () {
	$(".tampilModalUbah").on("click", function () {
		$("#exampleModalLabel").html("Edit Data");
		$(".modal-footer button[type=submit]").html("Edit");
		$(".modal-body form").attr("action", base_url + "master/ubahData");
		const id = $(this).data("id");
		$.ajax({
			url: base_url + "master/ambilData",
			data: {
				id: id,
			},
			method: "POST",
			dataType: "json",
			success: function (data) {
				$("#name").val(data.name);
				$("#phone").val(data.phone);
				$("#email").val(data.email);
				$("#address").val(data.address);
			},
		});
	});
});
