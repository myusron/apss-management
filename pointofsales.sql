-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 28, 2021 at 09:51 AM
-- Server version: 10.3.16-MariaDB
-- PHP Version: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pointofsales`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(11) NOT NULL,
  `categories` varchar(255) NOT NULL,
  `codecategories` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `master`
--

CREATE TABLE `master` (
  `id` mediumint(8) UNSIGNED NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `phone` varchar(100) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `master`
--

INSERT INTO `master` (`id`, `name`, `phone`, `email`, `address`) VALUES
(1, 'Markus Elton Harrison bro', '1-872-348-1267', 'ornare@outlook.net', 'P.O. Box 251, 8942 Tempus St.'),
(2, 'Yardley Hayden', '1-216-336-0468', 'tincidunt.nibh@icloud.org', 'Ap #442-9978 Elit. Rd.'),
(3, 'Baker Haley', '1-460-313-8127', 'sit.amet@yahoo.org', '548-6186 Interdum Avenue'),
(4, 'Nathan Parsons', '(327) 162-3918', 'malesuada.integer@google.edu', '730-5637 Donec Road'),
(5, 'Aiko Noble', '(738) 370-5586', 'non.leo@google.com', 'P.O. Box 750, 9790 Vitae Rd.'),
(6, 'Todd Buckley', '(485) 351-2487', 'ullamcorper.duis@protonmail.edu', 'P.O. Box 718, 2267 Parturient Rd.'),
(7, 'Kelly Clark', '1-986-488-1881', 'id.libero@icloud.couk', '2717 Interdum. Ave'),
(8, 'Echo Marshall', '(238) 540-3813', 'eu@yahoo.edu', 'P.O. Box 791, 9230 Egestas St.'),
(9, 'Sylvester Park', '1-257-861-2425', 'ipsum.leo@aol.couk', 'P.O. Box 119, 4477 Euismod Rd.'),
(10, 'Nathan Shields', '1-339-383-3871', 'non.luctus@aol.ca', '8744 Tincidunt Av.'),
(11, 'Renee Jarvis', '1-785-371-4815', 'aptent.taciti@hotmail.ca', 'Ap #454-3927 Erat, St.'),
(12, 'Alfonso Murray', '1-861-126-6077', 'nunc.mauris@outlook.couk', '6626 Mauris St.'),
(13, 'Simon May', '(672) 262-5248', 'non.sapien@aol.edu', 'Ap #627-177 Eu Ave'),
(14, 'Diana Rosales', '(758) 823-8108', 'per@aol.net', 'Ap #186-4135 Libero. Av.'),
(15, 'Kieran Raymond', '1-415-667-6534', 'purus.accumsan@icloud.org', 'Ap #312-1022 Consequat Road'),
(16, 'Kennan Coffey', '1-565-729-5777', 'rutrum.fusce.dolor@yahoo.ca', '248-1294 Nisi Ave'),
(17, 'Zia Meyer', '1-823-646-8374', 'urna.nullam.lobortis@outlook.net', 'P.O. Box 182, 9778 Nisi. Ave'),
(18, 'Ursa James', '1-766-643-4727', 'commodo@aol.net', 'P.O. Box 910, 4222 Facilisis. Av.'),
(19, 'Malachi Stark', '(578) 485-1639', 'lacinia.mattis.integer@icloud.edu', 'Ap #635-9781 Ligula St.'),
(20, 'Hall Thornton', '1-622-179-6373', 'feugiat.sed@icloud.org', '436-6281 Dui Ave'),
(21, 'Anonymous', '0812345678', 'test@test.com', 'jakarta'),
(22, 'User test', '08881112223', 'test@email.com', 'jakarta'),
(23, 'Muhammad Yusron', '0812345678', 'test@email.com', 'jakarta');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `role_id` int(11) NOT NULL,
  `is_active` int(11) NOT NULL,
  `date_created` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `name`, `email`, `image`, `password`, `role_id`, `is_active`, `date_created`) VALUES
(1, 'Superadmin', 'admin@admin.com', 'transistor-no-connection-2.png', '$2y$10$EcK5SKja50HdEWEseGUdXetHuxUMbNLvp6qeoSrsyLgNzbSDVf.Da', 1, 1, 1639146595),
(2, 'User biasa', 'user@user.com', 'png-transparent-the-c-programming-language-computer-programming-programming-miscellaneous-blue-computer-thumbnail-removebg-preview.png', '$2y$10$NeF6TVSp.6k7cfkOwOMCeOsmJ3375aLeKfArh5xrHbtiA7otFdBoy', 2, 1, 1639146950);

-- --------------------------------------------------------

--
-- Table structure for table `user_access_menu`
--

CREATE TABLE `user_access_menu` (
  `id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_access_menu`
--

INSERT INTO `user_access_menu` (`id`, `role_id`, `menu_id`) VALUES
(1, 1, 1),
(2, 2, 2),
(4, 1, 2),
(5, 1, 3),
(9, 1, 4),
(10, 1, 11),
(11, 1, 12),
(13, 2, 12),
(14, 1, 13);

-- --------------------------------------------------------

--
-- Table structure for table `user_menu`
--

CREATE TABLE `user_menu` (
  `id` int(11) NOT NULL,
  `menu` varchar(255) NOT NULL,
  `icon` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_menu`
--

INSERT INTO `user_menu` (`id`, `menu`, `icon`) VALUES
(1, 'Admin', 'fas fa-chart-pie'),
(2, 'User', 'fas fa-users'),
(3, 'Menu', 'fas fa-file'),
(4, 'Master', 'fas fa-th'),
(11, 'Products', 'fas fa-copy'),
(12, 'Transaction', 'fas fa-edit'),
(13, 'Reports', 'fas fa-book');

-- --------------------------------------------------------

--
-- Table structure for table `user_role`
--

CREATE TABLE `user_role` (
  `id` int(11) NOT NULL,
  `role` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_role`
--

INSERT INTO `user_role` (`id`, `role`) VALUES
(1, 'Admin'),
(2, 'User');

-- --------------------------------------------------------

--
-- Table structure for table `user_sub_menu`
--

CREATE TABLE `user_sub_menu` (
  `id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `url` varchar(255) NOT NULL,
  `iconsubmenu` varchar(255) NOT NULL,
  `is_active` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_sub_menu`
--

INSERT INTO `user_sub_menu` (`id`, `menu_id`, `title`, `url`, `iconsubmenu`, `is_active`) VALUES
(1, 1, 'Dashboard', 'admin', 'fas fa-tachometer-alt', 1),
(2, 3, 'Menu Management', 'menu', 'fas fa-folder', 1),
(3, 3, 'Submenu Management', 'menu/submenu', 'fa fa-folder-open', 1),
(4, 2, 'Profile', 'user', 'fas fa-user', 1),
(5, 1, 'Role', 'admin/role', 'fas fa-users', 1),
(6, 4, 'Master A', 'master', 'fas fa-table', 1),
(7, 10, 'test', 'test/test', 'testinging', 1),
(10, 4, 'Supliers', 'master/supliers', 'fas fa-plus-square', 1),
(11, 4, 'Customers', 'master/customers', 'fas fa-address-card', 1),
(12, 11, 'Categories', 'products/categories', 'fas fa-copy', 1),
(13, 11, 'Units', 'products/units', 'fas fa-suitcase-rolling', 1),
(14, 11, 'Items', 'products/items', 'fas fa-clipboard-list', 1),
(15, 12, 'Sales', 'transaction', 'fas fa-shopping-cart', 1),
(16, 12, 'Stok In', 'transaction/stokin', 'fas fa-arrow-circle-left', 1),
(17, 12, 'Stok Out', 'transaction/stokout', 'fas fa-arrow-circle-right', 1),
(18, 13, 'Sales', 'reports/sales', 'fas fa-list-ul', 1),
(19, 13, 'Stok In / Stok Out', 'reports/stokinout', 'fas fa-list-alt', 1),
(20, 1, 'List User', 'admin/listuser', 'fas fa-th-list', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `master`
--
ALTER TABLE `master`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_access_menu`
--
ALTER TABLE `user_access_menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_menu`
--
ALTER TABLE `user_menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_role`
--
ALTER TABLE `user_role`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_sub_menu`
--
ALTER TABLE `user_sub_menu`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `master`
--
ALTER TABLE `master`
  MODIFY `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `user_access_menu`
--
ALTER TABLE `user_access_menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `user_menu`
--
ALTER TABLE `user_menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `user_role`
--
ALTER TABLE `user_role`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `user_sub_menu`
--
ALTER TABLE `user_sub_menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
